# Proyecto SAM - INFRA CreditScore

Servicio Backend Lambda - API

## Capas

- [Front](#Front)
- [Servicios](#services) 

## Front

Explica en más detalle qué hace tu proyecto y por qué es útil. Añade algunas capturas de pantalla si es aplicable.

## Services

Lista las tecnologías, librerías y frameworks que usas en el proyecto.

 - Autenticación 

![Flujo Authentication](./images/autenticacion.png)

- Diagrama Secuencia

![Diagrama Secuencia](./images/secuencia.png)

- Modelo de Datos

![Modelo de Datos](./images/modelo.png)
