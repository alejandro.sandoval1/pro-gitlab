const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const PORT = process.env.PORT || 3001;

// Middleware para analizar el cuerpo de las solicitudes en formato JSON
app.use(bodyParser.json());

// Ruta de ejemplo para la API POST
// Endpoint para el login
app.post('/api/login', (req, res) => {
    const { username, password } = req.body;
  
    // Valida el usuario y la contraseña
    if (username === 'monica' && password === 'araneda') {
      res.json({ status: 'ok', message: 'Usuario autenticado correctamente' });
    } else {
      res.status(401).json({ status: 'error', message: 'Usuario o contraseña inválido' });
    }
  });

// Definir una clase Item (solo para referencia, no se usa directamente en Node.js)
class Item {
    constructor(nombre, iconName, isSelected, price, totalValue) {
      this.nombre = nombre;
      this.iconName = iconName;
      this.isSelected = isSelected;
      this.price = price;
      this.totalValue = totalValue;
    }
  }
  
  // Crear una lista de objetos Item
  const items = [
    new Item('Item1', 'icon1', false, 10.5, 100.0),
    new Item('Item2', 'icon2', false, 20.0, 200.0),
    new Item('Item3', 'icon3', false, 30.5, 300.0),
    new Item('Item4', 'icon4', false, 40.0, 400.0),
    new Item('Item5', 'icon5', false, 50.5, 500.0),
  ];
  
  // Endpoint para obtener la lista de objetos Item
  app.get('/api/items', (req, res) => {
    res.json(items);
  });

// Nueva ruta para /api/persona
app.post('/api/persona', (req, res) => {
    const jsonResponse = {
        transactionId: "cd8dfa54-ef2b-4345-83f3-7d2d6ec80999",
        status: "completed",
        exclusionaryConditions: {
            NotAvailable: false,
            ErrorsPresent: false
        },
        platinum360: {
            getInformePlatinum360Response: {
                response: {
                    Credit: {
                        Bank: {
                            TotalBank: 0,
                            BanksActive: []
                        },
                        ConsultRUT: {
                            InquiriesNumber: 11,
                            DetailConsultRUT: {
                                DetailConsultRUTType: [
                                    { InquiryDate: "20231114", InquiryCompanyName: "ADMINISTRADORA PLAZA S A", InquiryReportName: "INF COMPLEMENT MOR SIN SCORE" },
                                    { InquiryDate: "20231114", InquiryCompanyName: "ADMINISTRADORA PLAZA S A", InquiryReportName: "INF COMPLEMENT MOR SIN SCORE" },
                                    { InquiryDate: "20231114", InquiryCompanyName: "ADMINISTRADORA PLAZA S A", InquiryReportName: "INF COMPLEMENT MOR SIN SCORE" },
                                    { InquiryDate: "20231114", InquiryCompanyName: "ADMINISTRADORA PLAZA S A", InquiryReportName: "INF COMPLEMENT MOR SIN SCORE" },
                                    { InquiryDate: "20230524", InquiryCompanyName: "TRANSBANK SA", InquiryReportName: "INF COMPLEMENT MOR SIN SCORE" },
                                    { InquiryDate: "20230524", InquiryCompanyName: "TRANSBANK SA", InquiryReportName: "INFORME PLATINUM 360" },
                                    { InquiryDate: "20230328", InquiryCompanyName: "GENERAL MOTORS FINANCIAL CHILE SA", InquiryReportName: "INF COMPLEMENT MOR SIN SCORE" },
                                    { InquiryDate: "20230323", InquiryCompanyName: "SERVICIOS FINANCIEROS CONCRECES S A", InquiryReportName: "INF COMPLEMENT MOR SIN SCORE" },
                                    { InquiryDate: "20230328", InquiryCompanyName: "GENERAL MOTORS FINANCIAL CHILE SA", InquiryReportName: "INF COMPLEMENT MOR SIN SCORE" },
                                    { InquiryDate: "20230323", InquiryCompanyName: "SERVICIOS FINANCIEROS CONCRECES S A", InquiryReportName: "INF COMPLEMENT MOR SIN SCORE" },
                                    { InquiryDate: "20230316", InquiryCompanyName: "INVERSIONES SEPTIMA REGION LIMITADA", InquiryReportName: "INF COMPLEMENT MOR SIN SCORE" },
                                    { InquiryDate: "20230315", InquiryCompanyName: "GTD TELEDUCTOS S A", InquiryReportName: "INF COMPLEMENT MOR SIN SCORE" },
                                    { InquiryDate: "20230309", InquiryCompanyName: "PRIVILEGE SOCIEDAD ANONIMA", InquiryReportName: "INF COMPLEMENT MOR SIN SCORE" }
                                ]
                            },
                            TimelapseNumber: 6,
                            TimelapseTypeUsed: "MESES"
                        },
                        CreditRisk: {
                            PredictorScale: {
                                MaximumRate: 607,
                                MinimumRate: 460
                            },
                            ImportantAspect: [],
                            PersonsRate: 607,
                            Everclean: "0",
                            HistoricalRate: {
                                HistoricalRateType: [
                                    { Rate: 607, Period: 202312 },
                                    { Rate: 460, Period: 202311 },
                                    { Period: 202310 },
                                    { Rate: 460, Period: 202309 },
                                    { Period: 202308 },
                                    { Rate: 460, Period: 202307 },
                                    { Period: 202306 },
                                    { Rate: 460, Period: 202305 },
                                    { Period: 202304 },
                                    { Rate: 460, Period: 202303 },
                                    { Period: 202302 },
                                    { Rate: 460, Period: 202301 },
                                    { Period: 202212 },
                                    { Rate: 460, Period: 202211 },
                                    { Period: 202210 },
                                    { Rate: 460, Period: 202209 },
                                    { Period: 202208 },
                                    { Rate: 460, Period: 202207 },
                                    { Period: 202206 },
                                    { Rate: 460, Period: 202205 },
                                    { Period: 202204 },
                                    { Rate: 460, Period: 202203 },
                                    { Rate: 460, Period: 202202 },
                                    { Period: 202201 }
                                ]
                            },
                            lessPercentage: ""
                        },
                        DebtsSummary: {
                            UnpaidTotalAmount: 78941,
                            TotalDocsAccumulated12Months: 0,
                            TotalDocsAccumulatedMoreThan24Months: 0,
                            BOLAB: {
                                BOLABType: [],
                                bolabtype: []
                            },
                            DelinquencyNumber: 0,
                            ProtestSummary: {
                                ExpirationDateLastProtest: "0",
                                TotalProtests: 0,
                                AmountLastProtest: "0",
                                TotalAmountProtests: 0
                            },
                            TotalMultaeInfraccionLaboralPrevisional: 0,
                            UnpaidProtestDebtsThousandPesos: 0,
                            TotalDocsFrom6To12Months: 0,
                            UnpaidNumberInformed: 0,
                            DocsAmountAccumulated6Months: 78941,
                            TotalDocsICOM: 1,
                            DocsAmountAccumulated12Months: 0,
                            UnpaidLastAmount: 78941,
                            TotalDocsLast6Months: 1,
                            DebtsSummaryDetail: {
                                TotalAmountDebts: 78941,
                                TotalDebts: 1
                            },
                            ICOM: {
                                ICOMType: [
                                    {
                                        expirationDate: "20191205",
                                        documentType: "TC",
                                        moneyCode: "$",
                                        unpaidAmount: "78941",
                                        reasonType: "MCC",
                                        libradorName: "TARJETA ABCVISA",
                                        localidadName: "ESTACION CENTRAL",
                                        debtType: "D",
                                        chequeOperatioNumber: "2000",
                                        creditType: "ICOM",
                                        marketCode: "",
                                        marketDescription: ""
                                    }
                                ],
                                icomtype: [
                                    {
                                        expirationDate: "20191205",
                                        documentType: "TC",
                                        moneyCode: "$",
                                        unpaidAmount: "78941",
                                        reasonType: "MCC",
                                        libradorName: "TARJETA ABCVISA",
                                        localidadName: "ESTACION CENTRAL",
                                        debtType: "D",
                                        chequeOperatioNumber: "2000",
                                        creditType: "ICOM",
                                        marketCode: "",
                                        marketDescription: ""
                                    }
                                ]
                            },
                            UnpaidLastExpirationDate: "20191205",
                            ConectionAICSSErrorIndicator: "S",
                            UnpaidLastDebtType: "TC",
                            TotalDocsFrom12To24Months: 0,
                            TotalDocsBoletinProtestosEImpagos: 0,
                            BED: {
                                BEDType: [],
                                bedtype: []
                            },
                            AmountAccumulated6Months: 78941,
                            DocsAmountAccumulatedMoreThan24Months: 0,
                            UnpaidAmountPesos: 0,
                            DocsAmountAccumulated24Months: 78941,
                            AccessCamaraComercio: "SI",
                            DocsAmountLast6Months: 78941,
                            TotalDocsMoreThan24Months: 0,
                            DocsAmountFrom6To12Months: 0,
                            DocsAmountFrom12To24Months: 0,
                            SumaryBOLAB: {
                                TotalBolab: 0,
                                TotalAmountBolab: 0,
                                AmountLastBolab: "0"
                            },
                            DocsAmountMoreThan24Months: 0,
                            TotalDocsAccumulated24Months: 1,
                            BOLCOM: {
                                BOLCOMType: [],
                                bolcomtype: []
                            },
                            UnpaidTotalNumber: 1,
                            LastDateBoletinLaboral: "00000000"
                        },
                        BoletinConcursal: {
                            DetailBoletinConcursal: "0",
                            SumaryBolConcursal: {
                                PublicationNumberBolConcursal: "0"
                            }
                        },
                        SBIF: {
                            SBIFSummary: {
                                TotalDebts: "",
                                DetailsCount: 0,
                                PresenceOfDetails: "N",
                                LatestFinancialDebt: ""
                            },
                            SBIFDetail: []
                        }
                    },
                    Capacity: {
                        Salary: {
                            RentaFuncionarioPublico: "",
                            RentaPresunta: "",
                            DescCurrecy: "PESOS"
                        },
                        IncomeEstimate: {
                            DataEstimateType: [
                                { SourceName: "FINANCIERA INDIVIDUAL", Amount: "227931", ProductName: "CREDITO CONSUMO" },
                                { SourceName: "SERVICIOS FINANCIEROS CONCRECES S A", Amount: "454405", ProductName: "CREDITO CONSUMO" }
                            ]
                        }
                    },
                    NegativeData: {
                        JudicialCaus: {
                            JudicialType: []
                        },
                        DebtSII: {
                            SIISummary: {
                                TotalDetails: 0
                            },
                            DebtSIIDetail: []
                        },
                        Protest: {
                            TotalProtests: 0,
                            ProtestType: []
                        },
                        MultipleCivilCauses: "N",
                        LaborInfringement: {
                            TotalInfringements: 0,
                            LaborInfringementDetail: []
                        },
                        NegativeSummary: {
                            AmountTotalDebt: 78941,
                            NumberTotalDebt: 1
                        },
                        NegativeGeneralDetail: {
                            DetailDebtsType: [
                                {
                                    UnpaidAmount: "78941",
                                    ExpirationDate: "20191205",
                                    DebtType: "TC",
                                    DocumentType: "ICOM"
                                }
                            ]
                        },
                        ProtestDetail: []
                    },
                    CommercialSectors: {
                        Names: [
                            "FINANCIERA INDIVIDUAL",
                            "SERVICIOS FINANCIEROS CONCRECES S A"
                        ]
                    },
                    SalesSummary: {
                        LastPeriodDate: "202312",
                        LastPeriodSales: 6823365,
                        MonthlyAverage: 6823365,
                        HistoricalSales: {
                            HistoricalSalesType: [
                                { SalesAmount: 6823365, Period: "202312" }
                            ]
                        },
                        MonthlySalesDetail: {
                            MonthlySalesDetailType: [
                                { SalesAmount: 6823365, Period: "202312" }
                            ]
                        }
                    },
                    Properties: {
                        RealStateProperties: [],
                        Cars: []
                    },
                    Work: {
                        Jobs: [
                            {
                                WorkSituation: "INDEPENDIENTE",
                                CurrentJobFlag: "1",
                                JobActivity: "TRABAJADOR INDEPENDIENTE"
                            }
                        ],
                        IndividualActivity: []
                    },
                    PersonGeneral: {
                        HistoricalConnectionDate: "20230310",
                        HistoricalInquiries: 14,
                        Inquiries12Months: 14,
                        PersonType: "N",
                        GeneralInformation: {
                            PersonGeneralInfo: {
                                Address: {
                                    AddressType: [
                                        {
                                            LocationDescription: "REGION METROPOLITANA",
                                            Number: "02000",
                                            AddressData: "1"
                                        }
                                    ]
                                }
                            }
                        },
                        Emails: [],
                        Phones: [],
                        SocietyData: {
                            Societies: [],
                            Representative: []
                        },
                        Names: {
                            PersonName: "ALEJANDRO SANDOVAL"
                        }
                    },
                    MoroseSummary: {
                        LastDebtorStatus: "AL DIA",
                        TotalNumberOfDocs: 0,
                        MoroseNumberOfDocs: 0,
                        InquiriesType: [],
                        MoroseTotalAmount: 0,
                        NumberInquiries: 14,
                        LastPeriod: "202303"
                    }
                }
            }
        }
    };

    res.json(jsonResponse);
});

// Inicia el servidor
app.listen(PORT, () => {
    console.log(`Servidor corriendo en el puerto ${PORT}`);
});

