exports.informePersona = async (event) => {
    
const axios = require('axios');
 
let config = {
  method: 'get',
  maxBodyLength: Infinity,
  url: 'https://api.sandbox.equifax.com/personal/consumer-data-suite/v1/creditReport/bahdguy-efx-001/summary',
  headers: { 
    'Authorization': 'Bearer HBG2NLy_-e1ZJQNGBXU32dMX0jQ'
  }
};
 
try {
    const response = await axios.request(config);
    const data = response.data;
    console.log(JSON.stringify(data));
    
    return {
      statusCode: 200,
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: error.response ? error.response.status : 500,
      body: JSON.stringify({ message: error.message }),
      headers: {
        'Content-Type': 'application/json'
      }
    };
  }

};