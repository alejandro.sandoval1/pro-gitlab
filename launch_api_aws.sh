# Define las variables necesarias para la firma AWS
AWS_ACCESS_KEY="AKIAXC2ZDBBDYD3JWCLN"
AWS_SECRET_KEY="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"
AWS_REGION="us-east-1"  # por ejemplo, us-east-1
AWS_SERVICE="execute-api"

# Define la URL del endpoint de CloudFront que apunta a API Gateway
URL="https://d3mjvxs9hbk2qg.cloudfront.net/Prod/mycreditscore/api/login"

# Define la fecha y la fecha amz en formato ISO8601 (UTC)
DATE=$(date -u "+%Y%m%dT%H%M%SZ")
AMZ_DATE=$(date -u "+%Y%m%dT%H%M%SZ")

# Calcula los encabezados firmados y la firma
HEADERS="host:${URL#https://}"
REQUEST_HEADERS="host;x-amz-date"

# Crea el string de canonical request
HTTP_REQUEST_METHOD="POST"  # Método POST para el ejemplo proporcionado
CANONICAL_URI="/Prod/mycreditscore/api/login"
CANONICAL_QUERY_STRING=""
CANONICAL_HEADERS="host:${URL#https://}\nx-amz-date:${AMZ_DATE}\n"
CANONICAL_PAYLOAD='{"username":"Monica","password":"Araneda"}'
CANONICAL_REQUEST="${HTTP_REQUEST_METHOD}\n${CANONICAL_URI}\n${CANONICAL_QUERY_STRING}\n${CANONICAL_HEADERS}\n${REQUEST_HEADERS}\n$(echo -n "${CANONICAL_PAYLOAD}" | openssl sha256 -hex)"

# Calcula el hash SHA-256 del canonical request
HASHED_CANONICAL_REQUEST=$(echo -n "${CANONICAL_REQUEST}" | openssl sha256 -hex | cut -d ' ' -f 1)

# Calcula la firma AWS Signature Version 4
function sign() {
    key=$1
    dateStamp=$(echo -n $2 | cut -c1-8)
    region=$3
    service=$4
    signingKey=$(printf "AWS4%s" $key | xxd -p -c 256)
    kSecret=$(printf "%s" $dateStamp | openssl dgst -sha256 -mac HMAC -macopt hexkey:$signingKey -binary)
    kRegion=$(printf "%s" $region | openssl dgst -sha256 -mac HMAC -macopt hexkey:$kSecret -binary)
    kService=$(printf "%s" $service | openssl dgst -sha256 -mac HMAC -macopt hexkey:$kRegion -binary)
    kSigning=$(printf "aws4_request" | openssl dgst -sha256 -mac HMAC -macopt hexkey:$kService -binary)
    signature=$(printf "%s" $2 | openssl dgst -sha256 -mac HMAC -macopt hexkey:$kSigning | cut -d ' ' -f 2)
    echo $signature
}

SIGNATURE=$(sign $AWS_SECRET_KEY $AMZ_DATE $AWS_REGION $AWS_SERVICE)

# Construye el encabezado de autorización
AWS_ALGORITHM="AWS4-HMAC-SHA256"
SCOPE="${DATE}/${AWS_REGION}/${AWS_SERVICE}/aws4_request"
AUTH_HEADER="${AWS_ALGORITHM} Credential=${AWS_ACCESS_KEY}/${SCOPE}, SignedHeaders=${REQUEST_HEADERS}, Signature=${SIGNATURE}"

# Realiza la solicitud usando curl con el encabezado de autorización
curl --location --request POST "${URL}" \
--header "Authorization: ${AUTH_HEADER}" \
--header "x-amz-date: ${AMZ_DATE}" \
--header "Host: ${URL#https://}" \
--header "Content-Type: application/json" \
--data "${CANONICAL_PAYLOAD}"
