//
//  PayPalWebViewController.swift
//  MyCreditScore
//
//  Created by Monica Araneda on 09-06-24.
//

import Foundation
import UIKit
import WebKit


class WebPayViewController: UIViewController {

    
    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var rutTextField: UITextField!
    @IBOutlet weak var numeroTarjetaTextField: UITextField!
    @IBOutlet weak var fechaVigenciaTextField: UITextField!
    @IBOutlet weak var codigoSeguridadTextField: UITextField!
    @IBOutlet weak var webPayButton: UIButton!
    
    @IBOutlet weak var wKWebView: WKWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Configurar cualquier estilo adicional aquí
    }
    
    @IBAction func webPayButtonTapped(_ sender: UIButton) {
        if let url = URL(string: "https://www.webpay.cl") {
            UIApplication.shared.open(url)
        }
    }
}
