//
//  RegistroNextViewController.swift
//  MyCreditScore
//
//  Created by Monica Araneda on 10-06-24.
//

import Foundation
import UIKit

class RegisterStep2ViewController: UIViewController {

    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure the existing segmented control
        let items = ["1", "2", "3", "4"]
        segmentedControl.removeAllSegments()
        for (index, item) in items.enumerated() {
            segmentedControl.insertSegment(withTitle: item, at: index, animated: false)
        }
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.isUserInteractionEnabled = false
        
        
        updateSegmentedControl(step: 2)
        // Set up the title label
        // Calcular la posición Y para centrar los elementos verticalmente
        // Coordenadas centradas verticalmente en x: 51 y y: 245
        // Calcular las coordenadas para centrar los elementos verticalmente
        let centerY = self.view.frame.height / 2

        // Set up the title label
        let titleLabel = UILabel(frame: CGRect(x: 20, y: centerY - 200, width: self.view.frame.width - 40, height: 30))
        titleLabel.text = "Aviso"
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.boldSystemFont(ofSize: 24)
        self.view.addSubview(titleLabel)

        // Set up the icon image view
        let iconImageView = UIImageView(frame: CGRect(x: (self.view.frame.width - 50) / 2, y: centerY - 150, width: 50, height: 50))
        iconImageView.image = UIImage(systemName: "exclamationmark.triangle")
        iconImageView.tintColor = .systemRed
        self.view.addSubview(iconImageView)

        // Set up the text view
        let textView = UITextView(frame: CGRect(x: 20, y: centerY - 100, width: self.view.frame.width - 40, height: 200))
        textView.isEditable = false
        textView.text = "La ley nos exige validar tu identidad para protegerte de estafas y fraudes."
        textView.textAlignment = .center
        textView.font = UIFont.systemFont(ofSize: 16)
        self.view.addSubview(textView)

    }
    
    @IBAction func volver(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateSegmentedControl(step: Int) {
         for index in 0..<segmentedControl.numberOfSegments {
             segmentedControl.setEnabled(false, forSegmentAt: index)
         }
         segmentedControl.setEnabled(true, forSegmentAt: step - 1)
         segmentedControl.selectedSegmentIndex = step - 1
     }
    
  
    
    
}
