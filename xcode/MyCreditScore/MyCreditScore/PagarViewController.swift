//
//  PagarViewController.swift
//  MyCreditScore
//
//  Created by Alejandro Schwartz on 25-05-24.
//

import UIKit

class PagarViewController: UIViewController {

    @IBOutlet weak var labelError: UILabel!
    
    var totalAmount: Double = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()

        
        labelError.text = ""
        // Do any additional setup after loading the view.
    }
    
    @IBAction func volver() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
