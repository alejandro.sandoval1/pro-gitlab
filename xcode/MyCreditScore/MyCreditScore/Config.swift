//
//  Config.swift
//  MyCreditScore
//
//  Created by Monica Araneda on 15-06-24.
//

import Foundation
 
struct Config {
    static var apiEndpoint: String {
        guard let path = Bundle.main.path(forResource: "Config", ofType: "plist"),
              let xml = FileManager.default.contents(atPath: path),
              let plist = try? PropertyListSerialization.propertyList(from: xml, options: .mutableContainersAndLeaves, format: nil),
              let config = plist as? [String: Any],
              let url = config["APIEndpoint"] as? String else {
            fatalError("APIEndpoint not set in Config.plist")
        }
        return url
    }
}
