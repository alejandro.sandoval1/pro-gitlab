//
//  Item.swift
//  MyCreditScore
//
//  Created by Monica Araneda on 07-06-24.
//

import Foundation

class Item: Codable {
    var nombre: String
    var iconName: String
    var isSelected: Bool
    var totalValue: Double
    var price: Double?
    
    init(nombre: String, iconName: String, isSelected: Bool, price: Double, totalValue: Double) {
        self.nombre = nombre
        self.iconName = iconName
        self.isSelected = isSelected
        self.totalValue = totalValue
        self.price = price
    }
    

}
