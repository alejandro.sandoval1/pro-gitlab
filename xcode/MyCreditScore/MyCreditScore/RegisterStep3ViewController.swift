//
//  RegisterStep3ViewController.swift
//  MyCreditScore
//
//  Created by Monica Araneda on 11-06-24.
//

import Foundation
import UIKit
import AVFoundation // Importa el framework necesario para usar AVCaptureDevice
import Vision


class RegisterStep3ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var cedulaImageView: UIImageView!
    @IBOutlet weak var selfieImageView: UIImageView!
    @IBOutlet weak var cedulaTextLabel: UILabel!
    @IBOutlet weak var selfieTextLabel: UILabel!
    @IBOutlet weak var adviceLabel: UILabel!
    
    @IBOutlet weak var anversoImageView: UIImageView!
    @IBOutlet weak var reversoImageView: UIImageView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var validationLabel: UILabel!
    @IBOutlet weak var cancelar: UIButton!
    
    
    var anversoImage: UIImage? // Variable para almacenar la imagen del anverso
    var reversoImage: UIImage? // Variable para almacenar la imagen del reverso
    var selfieImage: UIImage? // Variable para almacenar la selfie
    var isCapturingAnverso = true // Variable para controlar cuál imagen se está capturando
    var isCapturingSelfie = false // Variable para controlar si se está capturando la selfie
    var selfieImageForComparison: UIImage? // Variable para almacenar la selfie solo para comparación

    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure the existing segmented control
        let items = ["1", "2", "3", "4"]
        segmentedControl.removeAllSegments()
        for (index, item) in items.enumerated() {
            segmentedControl.insertSegment(withTitle: item, at: index, animated: false)
        }
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.isUserInteractionEnabled = false
        
        updateSegmentedControl(step: 3)
        
        setupUI()
        addTapGestureToCedulaImageView()
        addTapGestureToSelfieImageView()
        
        // Get the position and size of the segmented control
        let segmentedControlFrame = segmentedControl.frame
        let validationLabelY = segmentedControlFrame.origin.y + segmentedControlFrame.size.height + 20
        
        // Configurar el texto del UILabel
        //validationLabel.text = "Empecemos a validar!\nUtiliza tu teléfono para tomar fotos, tardará menos de 2 minutos"
        //validationLabel.numberOfLines = 0  // Permite múltiples líneas
        //validationLabel.textAlignment = .center  // Centrar el texto si lo deseas
        //validationLabel.font = UIFont.boldSystemFont(ofSize: 16)
        //validationLabel.textColor = UIColor.black
        
        
    }
    
    func setupUI() {
        // Configure instruction label
        instructionLabel.text = "Empecemos a validar!\nUtiliza tu teléfono para tomar fotos, tardará menos de 2 minutos"
        instructionLabel.numberOfLines = 0
        instructionLabel.textAlignment = .center
        
        // Configure cedula image view
        if let cedulaImage = UIImage(systemName: "doc.text.viewfinder") {
            cedulaImageView.image = cedulaImage
        } else {
            print("Imagen 'doc.text.viewfinder' no encontrada")
        }
        cedulaImageView.contentMode = .scaleAspectFit
        
        // Configure selfie image view
        if let selfieImage = UIImage(systemName: "camera.fill") {
            selfieImageView.image = selfieImage
        } else {
            print("Imagen 'camera.fill' no encontrada")
        }
        selfieImageView.contentMode = .scaleAspectFit
        
        // Configure cedula text label
        cedulaTextLabel.text = "Ten a la mano tu cédula de identidad, le tomarás unas fotografías"
        cedulaTextLabel.numberOfLines = 0
        cedulaTextLabel.textAlignment = .center
        
        // Configure selfie text label
        selfieTextLabel.text = "Tómate una selfie. Para realizar el paso de seguridad de identidad"
        selfieTextLabel.numberOfLines = 0
        selfieTextLabel.textAlignment = .center
        
        // Configure advice label with icon and text
        adviceLabel.attributedText = createAttributedTextWithIcon()
        adviceLabel.numberOfLines = 0
        adviceLabel.textAlignment = .center
        
        // Configurar content mode para anverso y reverso UIImageView
        anversoImageView.contentMode = .scaleAspectFit
        reversoImageView.contentMode = .scaleAspectFit
    }
    
    func createAttributedTextWithIcon() -> NSAttributedString {
        let fullString = NSMutableAttributedString()
        
        // Create the attachment with the icon
        if let image = UIImage(systemName: "info.circle") { // Use an appropriate SF Symbol
            let tintedImage = image.withTintColor(.blue, renderingMode: .alwaysOriginal)
            let imageAttachment = NSTextAttachment()
            //imageAttachment.image = image
            imageAttachment.image = tintedImage
            imageAttachment.bounds = CGRect(x: 0, y: -3, width: 20, height: 20) // Adjust as needed
            let imageString = NSAttributedString(attachment: imageAttachment)
            fullString.append(imageString)
            fullString.append(NSAttributedString(string: " ")) // Space between icon and text
        } else {
            print("SF Symbol 'info.circle' no encontrado")
        }
        
        // Add the text
        let textString = NSAttributedString(string: "Consejo: Busca un lugar con buena iluminación")
        fullString.append(textString)
        
        return fullString
    }
    
    func addTapGestureToCedulaImageView() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(cedulaImageTapped))
        cedulaImageView.isUserInteractionEnabled = true
        cedulaImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func addTapGestureToSelfieImageView() {
           let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(selfieImageTapped))
           selfieImageView.isUserInteractionEnabled = true
           selfieImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func cedulaImageTapped() {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        switch status {
        case .authorized:
            presentCamera()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    DispatchQueue.main.async {
                        self.presentCamera()
                    }
                }
            }
        case .denied, .restricted:
            let alert = UIAlertController(title: "Acceso Denegado", message: "Por favor, habilita el acceso a la cámara en la configuración.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            present(alert, animated: true)
        @unknown default:
            break
        }
    }
    
    @objc func selfieImageTapped() {
        isCapturingSelfie = true
        presentCamera()
    }
    
    func presentCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = .camera
            present(imagePickerController, animated: true)
        } else {
            let alert = UIAlertController(title: "Cámara no disponible", message: "Este dispositivo no tiene una cámara disponible.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            present(alert, animated: true)
        }
    }
    
    // UIImagePickerControllerDelegate methods
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let image = info[.originalImage] as? UIImage {
                if isCapturingSelfie {
                    selfieImageForComparison = image
                    selfieImageView.image = selfieImageForComparison
                    isCapturingSelfie = false
                    
                    // Compara la selfie con la imagen del anverso de la cédula
                    if let anverso = anversoImage, let selfie = selfieImageForComparison {
                        compareFaces(anverso: anverso, selfie: selfie)
                    }
                } else if isCapturingAnverso {
                    // Guarda la imagen del anverso
                    anversoImage = image
                    // Asigna la imagen capturada a anversoImageView para mostrarla inmediatamente
                    anversoImageView.image = anversoImage
                    // Cambia la flag para capturar la imagen del reverso la próxima vez
                    isCapturingAnverso = false
                    
                    // Solicitar la captura de la imagen del reverso
                    let alert = UIAlertController(title: "Captura del Reverso", message: "Ahora, por favor, toma una foto del reverso de tu cédula.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                        self.presentCamera()
                    }))
                    present(alert, animated: true)
                } else {
                    // Guarda la imagen del reverso
                    reversoImage = image
                    // Asigna la imagen capturada a reversoImageView para mostrarla inmediatamente
                    reversoImageView.image = reversoImage
                    // Restablece la flag para la próxima captura
                    isCapturingAnverso = true
                    
                    // Aquí puedes realizar cualquier acción adicional con ambas imágenes
                    print("Imagen del anverso capturada: \(String(describing: anversoImage))")
                    print("Imagen del reverso capturada: \(String(describing: reversoImage))")
                }
            }
            picker.dismiss(animated: true, completion: nil)
        }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func compareFaces(anverso: UIImage, selfie: UIImage) {
        // Implementar la lógica para comparar las imágenes usando un detector de rostros
        // Aquí puedes usar Vision framework de Apple para detectar y comparar rostros
        
        let anversoCIImage = CIImage(image: anverso)!
        let selfieCIImage = CIImage(image: selfie)!
        
        let faceDetectionRequest = VNDetectFaceRectanglesRequest { (request, error) in
            guard let results = request.results as? [VNFaceObservation] else {
                return
            }
            // Comparar los resultados de la detección de rostros
            if results.count == 2 {
                // Aquí puedes implementar lógica para comparar los rostros detectados
                print("Dos rostros detectados, puedes proceder con la comparación")
            } else {
                print("La comparación falló, se encontraron \(results.count) rostros")
            }
        }
        
        let requestHandler = VNImageRequestHandler(ciImage: anversoCIImage, options: [:])
        do {
            try requestHandler.perform([faceDetectionRequest])
        } catch {
            print("Error en la detección de rostros: \(error)")
        }
    }

    func updateSegmentedControl(step: Int) {
         for index in 0..<segmentedControl.numberOfSegments {
             segmentedControl.setEnabled(false, forSegmentAt: index)
         }
         segmentedControl.setEnabled(true, forSegmentAt: step - 1)
         segmentedControl.selectedSegmentIndex = step - 1
     }
    
    
   func volver(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func exit(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func end(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
