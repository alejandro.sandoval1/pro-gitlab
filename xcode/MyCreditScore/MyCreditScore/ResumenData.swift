//
//  ResumenData.swift
//  MyCreditScore
//
//  Created by Monica Araneda on 20-06-24.
//

import Foundation



struct ResumenData: Codable {
    let nombre: String
    let gender: String // "M" for male, "F" for female
    let type: String // N natural E Empresa
    let creditScore: Int
    let creditStatus: String
    let utilization: String
    let paymentHistory: String
    let totalAccounts: Int
    let hardInquiries: Int
    let totalBalance: Double
    let isSubjectToCredit: Bool // Indica si la persona está sujeta a crédito
    let totalCredits: Int // Cantidad de créditos que tiene la persona
    let unpaidTotalAmount: Double // Monto total impago
    let totalDocsICOM: Int // Total de documentos ICOM
    let totalDocsLast6Months: Int // Total de documentos en los últimos 6 meses
    let creditData: CreditData
    let moroseSummary: MoroseSummary
}

struct CreditData: Codable {
    let isSubjectToCredit: Bool
    let totalCredits: Int
}

struct MoroseSummary: Codable {
    let lastDebtorStatus: String
    let totalNumberOfDocs: Int
    let moroseNumberOfDocs: Int
    let moroseTotalAmount: Double
    let numberInquiries: Int
    let lastPeriod: String
}
