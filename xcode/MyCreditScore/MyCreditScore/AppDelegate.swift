import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Crear la ventana principal
        window = UIWindow(frame: UIScreen.main.bounds)
        
        // Crear una instancia de LoadingViewController
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let loadingViewController = storyboard.instantiateViewController(withIdentifier: "LoadingViewController") as? LoadingViewController else {
            fatalError("LoadingViewController no se encontró en el storyboard.")
        }
        
        // Asignar LoadingViewController como rootViewController temporal
        window?.rootViewController = loadingViewController
        window?.makeKeyAndVisible()
        
        // Simular una carga de datos
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            // Después de 2 segundos, cambiar al verdadero rootViewController
            guard let mainViewController = storyboard.instantiateViewController(withIdentifier: "InicioViewController") as? InicioViewController else {
                fatalError("MainViewController no se encontró en el storyboard.")
            }
            self.window?.rootViewController = mainViewController
        }
        
        return true
    }

    // Otros métodos del AppDelegate
}
