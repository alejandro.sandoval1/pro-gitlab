import UIKit

class ReportTableViewCell: UITableViewCell {

    let numeroLabel = UILabel()
       let fechaMovimientoLabel = UILabel()
       let tipoMovimientoLabel = UILabel()
       let tipoDocumentoLabel = UILabel()
       let cantidadDocumentosLabel = UILabel()
       let emisorLabel = UILabel()
       let baseDatosLabel = UILabel()
       
       override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
           super.init(style: style, reuseIdentifier: reuseIdentifier)
           
           contentView.addSubview(numeroLabel)
           contentView.addSubview(fechaMovimientoLabel)
           contentView.addSubview(tipoMovimientoLabel)
           contentView.addSubview(tipoDocumentoLabel)
           contentView.addSubview(cantidadDocumentosLabel)
           contentView.addSubview(emisorLabel)
           contentView.addSubview(baseDatosLabel)
           
           numeroLabel.translatesAutoresizingMaskIntoConstraints = false
           fechaMovimientoLabel.translatesAutoresizingMaskIntoConstraints = false
           tipoMovimientoLabel.translatesAutoresizingMaskIntoConstraints = false
           tipoDocumentoLabel.translatesAutoresizingMaskIntoConstraints = false
           cantidadDocumentosLabel.translatesAutoresizingMaskIntoConstraints = false
           emisorLabel.translatesAutoresizingMaskIntoConstraints = false
           baseDatosLabel.translatesAutoresizingMaskIntoConstraints = false
           
           NSLayoutConstraint.activate([
               numeroLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
               numeroLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
               numeroLabel.widthAnchor.constraint(equalToConstant: 50),
               
               fechaMovimientoLabel.leadingAnchor.constraint(equalTo: numeroLabel.trailingAnchor, constant: 8),
               fechaMovimientoLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
               fechaMovimientoLabel.widthAnchor.constraint(equalToConstant: 100),
               
               tipoMovimientoLabel.leadingAnchor.constraint(equalTo: fechaMovimientoLabel.trailingAnchor, constant: 8),
               tipoMovimientoLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
               tipoMovimientoLabel.widthAnchor.constraint(equalToConstant: 100),
               
               tipoDocumentoLabel.leadingAnchor.constraint(equalTo: tipoMovimientoLabel.trailingAnchor, constant: 8),
               tipoDocumentoLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
               tipoDocumentoLabel.widthAnchor.constraint(equalToConstant: 100),
               
               cantidadDocumentosLabel.leadingAnchor.constraint(equalTo: tipoDocumentoLabel.trailingAnchor, constant: 8),
               cantidadDocumentosLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
               cantidadDocumentosLabel.widthAnchor.constraint(equalToConstant: 150),
               
               emisorLabel.leadingAnchor.constraint(equalTo: cantidadDocumentosLabel.trailingAnchor, constant: 8),
               emisorLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
               emisorLabel.widthAnchor.constraint(equalToConstant: 150),
               
               baseDatosLabel.leadingAnchor.constraint(equalTo: emisorLabel.trailingAnchor, constant: 8),
               baseDatosLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
               baseDatosLabel.widthAnchor.constraint(equalToConstant: 150)
           ])
       }
       
       required init?(coder: NSCoder) {
           fatalError("init(coder:) has not been implemented")
       }
}
