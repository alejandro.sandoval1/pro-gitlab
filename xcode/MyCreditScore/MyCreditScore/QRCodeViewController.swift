//
//  QRCodeViewController.swift
//  MyCreditScore
//
//  Created by Monica Araneda on 09-06-24.
//

import Foundation
import UIKit

class QRCodeViewController: UIViewController {
 

    @IBOutlet weak var qrCodeImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Aquí deberías asignar la imagen QR a qrCodeImageView
        qrCodeImageView.image = UIImage(named: "your_qr_code_image")
    }
}
