//
//  ResumenService.swift
//  MyCreditScore
//
//  Created by Monica Araneda on 20-06-24.
//

import Foundation




class ResumenService {
    
    var reportItems: [ReportItem] = []
    
    func fetchReportItems(for rut: String, token: String, completion: @escaping ([ReportItem]) -> Void) {
        let baseURL = Config.apiEndpoint
        guard let url = URL(string: "\(baseURL)/api/persona") else { return }
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(token, forHTTPHeaderField: "Authorization")
            
            let parameters: [String: Any] = ["rut": rut]
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    print("Network error: \(String(describing: error))")
                    return
                }
                
                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                    var reportItems: [ReportItem] = []
                    
                    if let platinum360 = jsonResponse?["platinum360"] as? [String: Any],
                       let getInformePlatinum360Response = platinum360["getInformePlatinum360Response"] as? [String: Any],
                       let response = getInformePlatinum360Response["response"] as? [String: Any],
                       let credit = response["Credit"] as? [String: Any],
                       let consultRUT = credit["ConsultRUT"] as? [String: Any],
                       let detailConsultRUT = consultRUT["DetailConsultRUT"] as? [String: Any],
                       let detailConsultRUTType = detailConsultRUT["DetailConsultRUTType"] as? [[String: Any]] {
                        
                        for detail in detailConsultRUTType {
                            if let inquiryDate = detail["InquiryDate"] as? String,
                               let inquiryCompanyName = detail["InquiryCompanyName"] as? String,
                               let inquiryReportName = detail["InquiryReportName"] as? String {
                                
                                let reportItem = ReportItem(
                                    fechaMovimiento: inquiryDate,
                                    tipoMovimiento: "Consulta",
                                    tipoDocumento: inquiryReportName,
                                    cantidadDocumentos: "1",
                                    emisor: inquiryCompanyName,
                                    baseDatos: "Platinum360"
                                )
                                reportItems.append(reportItem)
                            }
                        }
                    }
                    
                    completion(reportItems)
                } catch {
                    print("JSON decoding error: \(error)")
                }
            }
            
            task.resume()
        }
    
    /// <#Description#>
    /// - Parameters:
    ///   - rut: <#rut description#>
    ///   - token: <#token description#>
    ///   - completion: <#completion description#>
    func fetchResumen(for rut: String, token: String, completion: @escaping (ResumenData?) -> Void) {
        let baseURL = Config.apiEndpoint
        guard let url = URL(string: "\(baseURL)/api/persona") else { return }
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(token, forHTTPHeaderField: "Authorization")
            
            let parameters: [String: Any] = ["rut": rut]
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    print("Error fetching data: \(error?.localizedDescription ?? "Unknown error")")
                    completion(nil)
                    return
                }
                
                do {
                    if let jsonResponse = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        let resumenData = self.parseResumenData(from: jsonResponse)
                        completion(resumenData)
                    } else {
                        completion(nil)
                    }
                } catch {
                    print("Error parsing JSON: \(error.localizedDescription)")
                    completion(nil)
                }
            }
            
            task.resume()
        }
    
 
    
    /// <#Description#>
    /// - Parameter jsonResponse: <#jsonResponse description#>
    /// - Returns: <#description#>
    func parseResumenData(from jsonResponse: [String: Any]) -> ResumenData? {
        // Utilizar if let para extraer los datos necesarios de forma segura
        if let platinum360 = jsonResponse["platinum360"] as? [String: Any],
           let getInformePlatinum360Response = platinum360["getInformePlatinum360Response"] as? [String: Any],
           let response = getInformePlatinum360Response["response"] as? [String: Any],
           let credit = response["Credit"] as? [String: Any],
           let creditRisk = credit["CreditRisk"] as? [String: Any] {
            
            // Extraer información de personGeneral y creditRisk
            let personGeneral = response["PersonGeneral"] as? [String: Any]
            let type = personGeneral?["PersonType"] as? String ?? "N" // Assuming "N" for Natural, E Enterpresure
            let names = personGeneral?["Names"] as? [String: Any]
            let nombre = names?["PersonName"] as? String ?? "Usuario"
            let gender = "M" // Determinar el género basado en la información del JSON si está disponible
            let creditScore = creditRisk["PersonsRate"] as? Int ?? 0
            let creditStatus = creditRisk["lessPercentage"] as? String ?? "Desconocido"
            
            // Obtener el resumen de deudas
            let debtsSummary = credit["DebtsSummary"] as? [String: Any]
            let unpaidTotalAmount = debtsSummary?["UnpaidTotalAmount"] as? Double ?? 0.0
            let totalDocsICOM = debtsSummary?["TotalDocsICOM"] as? Int ?? 0
            let totalDocsLast6Months = debtsSummary?["TotalDocsLast6Months"] as? Int ?? 0
            
            // Calcular isSubjectToCredit y totalCredits
            let isSubjectToCredit = unpaidTotalAmount > 0
            let totalCredits = totalDocsICOM // Basado en la lógica de negocio, ajustar si es necesario
            
            // Calcular utilización de crédito e historial de pagos
            let (utilization, paymentHistory) = calculateUtilizationAndPaymentHistory(from: jsonResponse) ?? ("0.00%", "0.00%")
            
            // Calcular totalAccounts y hardInquiries
            let (totalAccounts, hardInquiries) = calculateTotalAccountsAndHardInquiries(from: jsonResponse) ?? (0, 0)
            
            let totalBalance = unpaidTotalAmount // Ajustar según sea necesario

            let creditData = CreditData(isSubjectToCredit: isSubjectToCredit, totalCredits: totalCredits)
            
            // Extraer datos de MoroseSummary
           let moroseSummaryDict = response["MoroseSummary"] as? [String: Any]
           let moroseSummary = MoroseSummary(
               lastDebtorStatus: moroseSummaryDict?["LastDebtorStatus"] as? String ?? "Desconocido",
               totalNumberOfDocs: moroseSummaryDict?["TotalNumberOfDocs"] as? Int ?? 0,
               moroseNumberOfDocs: moroseSummaryDict?["MoroseNumberOfDocs"] as? Int ?? 0,
               moroseTotalAmount: moroseSummaryDict?["MoroseTotalAmount"] as? Double ?? 0.0,
               numberInquiries: moroseSummaryDict?["NumberInquiries"] as? Int ?? 0,
               lastPeriod: moroseSummaryDict?["LastPeriod"] as? String ?? "Desconocido"
           )
            
            // Imprimir todos los valores rescatados para depuración
            print("Nombre: \(nombre)")
            print("Género: \(gender)")
            print("Tipo: \(type)")
            print("Credit Score: \(creditScore)")
            print("Credit Status: \(creditStatus)")
            print("Unpaid Total Amount: \(unpaidTotalAmount)")
            print("Total Docs ICOM: \(totalDocsICOM)")
            print("Total Docs Last 6 Months: \(totalDocsLast6Months)")
            print("Is Subject to Credit: \(isSubjectToCredit)")
            print("Total Credits: \(totalCredits)")
            print("Utilization: \(utilization)")
            print("Payment History: \(paymentHistory)")
            print("Total Accounts: \(totalAccounts)")
            print("Hard Inquiries: \(hardInquiries)")
            print("Total Balance: \(totalBalance)")
            
            print("Morose Number of Docs: \(moroseSummary.moroseNumberOfDocs)")
            print("Morose Total Amount: \(moroseSummary.moroseTotalAmount)")
            print("Number Inquiries: \(moroseSummary.numberInquiries)")
            print("Last Period: \(moroseSummary.lastPeriod)")
            
            return ResumenData(
                nombre: nombre,
                gender: gender,
                type: type,
                creditScore: creditScore,
                creditStatus: creditStatus,
                utilization: utilization,
                paymentHistory: paymentHistory,
                totalAccounts: totalAccounts,
                hardInquiries: hardInquiries,
                totalBalance: totalBalance,
                isSubjectToCredit: isSubjectToCredit,
                totalCredits: totalCredits,
                unpaidTotalAmount: unpaidTotalAmount,
                totalDocsICOM: totalDocsICOM,
                totalDocsLast6Months: totalDocsLast6Months,
                creditData: creditData,
                moroseSummary: moroseSummary
            )
        } else {
            print("Error: No se pudo extraer la información necesaria del JSON.")
            return nil
        }
    }



    // Historial de Pagos
    // El historial de pagos puede calcularse como el porcentaje de pagos realizados a tiempo. Esto generalmente se obtiene dividiendo el número de pagos realizados a tiempo por el número total de pagos y multiplicando por 100.

    //Voy a suponer que en tu JSON, estos valores se pueden calcular utilizando DebtsSummary, TotalAmountDebts, y TotalDocsLast6Months. Ajusta las propiedades según sea necesario.
    /// <#Description#>
    /// - Parameter jsonResponse: <#jsonResponse description#>
    /// - Returns: <#description#>
    func calculateUtilizationAndPaymentHistory(from jsonResponse: [String: Any]) -> (utilization: String, paymentHistory: String)? {
        guard let platinum360 = jsonResponse["platinum360"] as? [String: Any],
              let response = platinum360["getInformePlatinum360Response"] as? [String: Any],
              let credit = platinum360["Credit"] as? [String: Any],
              let debtsSummary = credit["DebtsSummary"] as? [String: Any] else {
            return nil
        }
        print("calculateUtilizationAndPaymentHistory")
        // Obtener el monto total de deudas impagas
        let unpaidTotalAmount = debtsSummary["UnpaidTotalAmount"] as? Double ?? 0.0

        // Supongamos que el límite total de crédito es parte de la estructura
        let totalCreditLimit = 100000.0 // Ajusta esto según los datos reales disponibles

        // Calcular la utilización de crédito
        let utilizationPercentage = (unpaidTotalAmount / totalCreditLimit) * 100
        let utilization = String(format: "%.2f%%", utilizationPercentage)
        
        // Obtener el total de documentos en los últimos 6 meses
        let totalDocsLast6Months = debtsSummary["TotalDocsLast6Months"] as? Int ?? 0

        // Obtener el total de documentos ICOM
        let totalDocsICOM = debtsSummary["TotalDocsICOM"] as? Int ?? 0

        // Supongamos que totalDocsLast6Months representa el total de pagos realizados y totalDocsICOM representa los pagos realizados a tiempo.
        let paymentsOnTime = totalDocsICOM
        let totalPayments = totalDocsLast6Months
        
        // Calcular el historial de pagos
        let paymentHistoryPercentage: Double
        if totalPayments > 0 {
            paymentHistoryPercentage = (Double(paymentsOnTime) / Double(totalPayments)) * 100
        } else {
            paymentHistoryPercentage = 100.0 // Si no hay pagos registrados, asumimos un 100%
        }
        let paymentHistory = String(format: "%.2f%%", paymentHistoryPercentage)

        return (utilization, paymentHistory)
    }

   // Voy a asumir que totalAccounts puede ser calculado como la suma de las cuentas de bancos y otros documentos de deuda. Ajusta esto según la estructura real del JSON.
    func calculateTotalAccountsAndHardInquiries(from jsonResponse: [String: Any]) -> (totalAccounts: Int, hardInquiries: Int)? {
        guard let platinum360 = jsonResponse["platinum360"] as? [String: Any],
              let getInformePlatinum360Response = platinum360["getInformePlatinum360Response"] as? [String: Any],
              let response = getInformePlatinum360Response["response"] as? [String: Any],
              let credit = response["Credit"] as? [String: Any],
              let consultRUT = credit["ConsultRUT"] as? [String: Any],
              let bank = credit["Bank"] as? [String: Any],
              let debtsSummary = credit["DebtsSummary"] as? [String: Any] else {
            return nil
        }
        print("calculateTotalAccountsAndHardInquiries")
        // Calcular totalAccounts
        let banksActiveCount = (bank["BanksActive"] as? [[String: Any]])?.count ?? 0
        let totalDocsLast6Months = debtsSummary["TotalDocsLast6Months"] as? Int ?? 0
        let totalAccounts = banksActiveCount + totalDocsLast6Months
        
        // Calcular hardInquiries
        let hardInquiries = consultRUT["InquiriesNumber"] as? Int ?? 0
        
        return (totalAccounts, hardInquiries)
    }


    
}
