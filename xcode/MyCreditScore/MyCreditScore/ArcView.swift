//
//  ArcView.swift
//  MyCreditScore
//
//  Created by Monica Araneda on 15-06-24.
//

import Foundation
import UIKit

class ArcView: UIView {
    var score: CGFloat = 0.0 { didSet { setNeedsDisplay() } }
    var maxScore: CGFloat = 1000.0
    var greenColor: UIColor = .green
    var redColor: UIColor = .red

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        let center = CGPoint(x: rect.width / 2, y: rect.height / 2)
        let radius = min(rect.width, rect.height) / 2 - 10
        let startAngle = CGFloat(-0.25 * .pi)
        let endAngle = startAngle + (2 * .pi * (score / maxScore))
        
        context.setLineWidth(20)
        
        // Green arc
        context.setStrokeColor(greenColor.cgColor)
        context.addArc(center: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: false)
        context.strokePath()
        
        // Red arc
        context.setStrokeColor(redColor.cgColor)
        context.addArc(center: center, radius: radius, startAngle: endAngle, endAngle: startAngle + 2 * .pi, clockwise: false)
        context.strokePath()
    }
}
