//
//  AccesoViewController.swift
//  MyCreditScore
//
//  Created by Alejandro Schwartz on 25-05-24.
//

import UIKit


class AccesoViewController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var errorLbl: UILabel!
    @IBOutlet weak var userTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var paswwordForgot: UIButton!
    @IBOutlet weak var LearningMore: UIButton!
    @IBOutlet weak var needHelp: UIButton!
    
    let motivoPicker = UIPickerView()
    
    let motivoOptions = ["Reclamo", "Consulta", "Soporte"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        motivoPicker.delegate = self
        motivoPicker.dataSource = self
        errorLbl.isHidden = true // Esconde la etiqueta al cargar la vista
        loginButton.isEnabled = false // Deshabilitar botón por defecto
        
        // Agregar observadores a los campos de texto
        userTF.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        passwordTF.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    @objc func textFieldDidChange() {
           // Verificar si ambos campos tienen texto
           if let username = userTF.text, let password = passwordTF.text {
               loginButton.isEnabled = !username.isEmpty && !password.isEmpty
           }
       }
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        print("Inicio Aceptar")
        errorLbl.isHidden = true
   
        guard let user = userTF.text, let password = passwordTF.text else {
            mostrarError(mensaje: "Por favor, ingrese usuario y contraseña.")
            return
        }
        
        if user.count < 6 || password.count < 6 {
              mostrarError(mensaje: "El usuario y la contraseña deben tener al menos 6 caracteres.")
              return
        }
        
        print("Usuario: $$user)")
        print("Contraseña: $$password)")
        
        let parametros: [String: Any] = ["username": user, "password": password]
        
        realizarSolicitudDeLogin(parametros: parametros)
        
    }
    
    func realizarSolicitudDeLogin(parametros: [String: Any]) {
        let baseURL = Config.apiEndpoint
        //guard let url = URL(string: "http://localhost:3001/login") else { return }
        guard let url = URL(string: "\(baseURL)/api/login") else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parametros, options: [])
        } catch {
            print("Error al crear el cuerpo de la solicitud: $$error)")
            mostrarError(mensaje: "Error al crear la solicitud.")
            return
        }
        
        realizarSolicitud(request: request)
    }
    
    func realizarSolicitud(request: URLRequest) {
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                print("Error en la solicitud: \(error)")
                self.mostrarError(mensaje: "Error en la solicitud: \(error.localizedDescription)")
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                print("Respuesta inválida")
                self.mostrarError(mensaje: "Respuesta inválida")
                return
            }
            
            self.procesarRespuesta(data: data ?? Data(), statusCode: httpResponse.statusCode)
        }
        
        task.resume()
    }


    func procesarRespuesta(data: Data, statusCode: Int) {
        DispatchQueue.main.async {
            if statusCode == 200 {
                do {
                    if let jsonResponse = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        let message = jsonResponse["message"] as? String
                        print("Mensaje: \(message ?? "Sin mensaje")")
                        self.performSegue(withIdentifier: "ServiciosViewController", sender: nil)
                    } else {
                        self.mostrarError(mensaje: "Error al procesar la respuesta")
                    }
                } catch {
                    self.mostrarError(mensaje: "Error al procesar la respuesta: \(error.localizedDescription)")
                }
            } else {
                self.mostrarError(mensaje: "Login failed with status code: \(statusCode)")
            }
        }
    }

    @IBAction func passwordForgotTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Recuperar Contraseña", message: "Seleccione el método de recuperación", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Email", style: .default, handler: { action in
            // Código para el método de recuperación por email
            self.presentEmailRecovery()
        }))
        
        alert.addAction(UIAlertAction(title: "RUT", style: .default, handler: { action in
            // Código para el método de recuperación por RUT
            self.presentRUTRecovery()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    func presentEmailRecovery() {
        // Código para presentar la modal de recuperación por email
        let emailAlert = UIAlertController(title: "Recuperar por Email", message: "Ingrese su dirección de email", preferredStyle: .alert)
        
        emailAlert.addTextField { textField in
            textField.placeholder = "Email"
        }
        
        emailAlert.addAction(UIAlertAction(title: "Enviar", style: .default, handler: { action in
            if let email = emailAlert.textFields?.first?.text {
                // Aquí puedes enviar el email para recuperación
                print("Enviar email de recuperación a: \(email)")
            }
        }))
        
        emailAlert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        self.present(emailAlert, animated: true, completion: nil)
    }

    func presentRUTRecovery() {
        // Código para presentar la modal de recuperación por RUT
        let rutAlert = UIAlertController(title: "Recuperar por RUT", message: "Ingrese su RUT", preferredStyle: .alert)
        
        rutAlert.addTextField { textField in
            textField.placeholder = "RUT"
        }
        
        rutAlert.addAction(UIAlertAction(title: "Enviar", style: .default, handler: { action in
            if let rut = rutAlert.textFields?.first?.text {
                // Aquí puedes enviar el RUT para recuperación
                print("Enviar RUT de recuperación: \(rut)")
            }
        }))
        
        rutAlert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        self.present(rutAlert, animated: true, completion: nil)
    }

    
    @IBAction func passwordForgot(_ sender: Any) {
        let alert = UIAlertController(title: "Recuperar Contraseña", message: "Seleccione el método de recuperación", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Recuperar por Email", style: .default, handler: { action in
            self.showTextFieldAlert(for: "Email")
        }))
        
        alert.addAction(UIAlertAction(title: "Recuperar por RUT", style: .default, handler: { action in
            self.showTextFieldAlert(for: "RUT")
        }))
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    func showTextFieldAlert(for method: String) {
        let alert = UIAlertController(title: "Ingrese \(method)", message: nil, preferredStyle: .alert)
        
        alert.addTextField { textField in
            textField.placeholder = method
        }
        
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { action in
            if let textField = alert.textFields?.first, let text = textField.text {
                // Aquí puedes realizar las acciones necesarias para recuperar la contraseña
                print("Método de recuperación: \(method), Valor ingresado: \(text)")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Servicios View Controller" {
            // Aquí puedes configurar la siguiente pantalla, por ejemplo:
            if segue.destination is ServiciosViewController {
                // Puedes pasar datos al siguiente ViewController si es necesario
                //ServiciosViewController.datos = "Algunos datos"
                print("Se ejecuto en prepare")
            }
        }
    }
    
    @IBAction func learnMoreTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Importante", message: "Para ingresar a esta aplicación primero debe registrarse.", preferredStyle: .alert)
           
           let okAction = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
           alertController.addAction(okAction)
           
           present(alertController, animated: true, completion: nil)
           
    }
 
    @IBAction func needHelpTapped(_ sender: Any) {
        showContactForm()
    }

    func showContactForm() {
        let contactForm = UIAlertController(title: "Formulario de Contacto", message: "Complete el formulario para enviar su mensaje", preferredStyle: .alert)
        
        contactForm.addTextField { textField in
            textField.placeholder = "Nombre"
        }
        
        contactForm.addTextField { textField in
            textField.placeholder = "Email"
            textField.keyboardType = .emailAddress
        }
        
        contactForm.addTextField { textField in
            textField.placeholder = "Mensaje"
        }
        
       // let motivoPicker = UIPickerView()
       // motivoPicker.delegate = self
       // motivoPicker.dataSource = self
       // motivoPicker.frame = CGRect(x: 0, y: 0, width: 250, height: 150)
        
       // let motivoPickerContainer = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 150))
       // motivoPickerContainer.addSubview(motivoPicker)
        
       // contactForm.view.addSubview(motivoPickerContainer)
        
      //  let motivoTextField = UITextField()
      //  motivoTextField.inputView = motivoPicker
      //  contactForm.addTextField { textField in
      //      motivoTextField.placeholder = "Motivo"
      //      motivoTextField.inputView = motivoPicker
        //}
        
        contactForm.addAction(UIAlertAction(title: "Enviar", style: .default, handler: { action in
            // Aquí puedes enviar el mensaje del formulario
        }))
        
        contactForm.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        self.present(contactForm, animated: true, completion: nil)
    }


    func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return motivoOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let motivo = motivoOptions[row]
        let maxLength = 10 // Longitud máxima del motivo en el picker
        if motivo.count > maxLength {
            return String(motivo.prefix(maxLength)) + "..." // Acortar y añadir puntos suspensivos
        } else {
            return motivo
        }
    }

    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // Aquí puedes capturar la selección del usuario si es necesario
    }

    @IBAction func volver(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func mostrarError(mensaje: String) {
        errorLbl.textColor = .red
        errorLbl.text = mensaje
        errorLbl.isHidden = false
        
        // Volver a la vista anterior
        self.dismiss(animated: true, completion: nil)
    }
    
    func mostrarMensajeExito() {
        DispatchQueue.main.async {
            self.errorLbl.isHidden = false
            self.errorLbl.textColor = .green
            self.errorLbl.text = "Mensaje: Login successful"
        }
    }
    
    
}

