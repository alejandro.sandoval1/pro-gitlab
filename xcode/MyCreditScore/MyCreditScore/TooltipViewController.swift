//
//  TooltipViewController.swift
//  MyCreditScore
//
//  Created by Monica Araneda on 09-06-24.
//

import Foundation
import UIKit

class TooltipViewController: UIViewController {
    
    
    @IBOutlet weak var messageLabel: UILabel!
    
    var message: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let message = message {
            messageLabel.text = message
        }
    }
}
