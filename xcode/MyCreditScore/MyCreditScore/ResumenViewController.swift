//
//  ResumenViewController.swift
//  MyCreditScore
//
//  Created by Alejandro Schwartz on 25-05-24.
//

import UIKit


class ResumenViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var fechaMovimientoHeaderLabel: UILabel!
    @IBOutlet weak var tipoMovimientoHeaderLabel: UILabel!
    @IBOutlet weak var tipoDocumentoHeaderLabel: UILabel!
    @IBOutlet weak var cantidadDocumentosHeaderLabel: UILabel!
    @IBOutlet weak var emisorHeaderLabel: UILabel!
    @IBOutlet weak var baseDatosHeaderLabel: UILabel!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    
    // Outlets para el pie de la tabla
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var totalRecordsLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    var paginatedItems: [ReportItem] = []
    let resumenService = ResumenService()
    let numeroHeaderLabel = UILabel()
    var reportItems: [ReportItem] = []
    let pageStackView = UIStackView()
    
    var currentPage: Int = 1
    let pageSize: Int = 10
    var totalPages: Int {
          return Int(ceil(Double(reportItems.count) / Double(pageSize)))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configurar los datos del reporte
        configureReportData()
       
       // Registrar la célula personalizada
        tableView.register(ReportTableViewCell.self, forCellReuseIdentifier: "CustomReportCell")
       
        // Configurar la tabla
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        // Permitir el desplazamiento horizontal
        tableView.isScrollEnabled = true
        tableView.showsHorizontalScrollIndicator = true
        tableView.alwaysBounceHorizontal = true
        
        // Configurar la cabecera
        configureTableHeader()
        
        // Configurar el pie de tabla
        configureTableFooter()
        
        // Agregar constraints para la tabla
        setupTableViewConstraints()
        
        // Cargar los datos de la primera página
        loadPage(page: currentPage)

       // Verificar que los datos se han cargado correctamente
       print("Datos cargados en viewDidLoad: \(reportItems.count) ítems")

    }

 
    func configureReportData() {
        let rut = "your_rut_here"  // Replace with the actual RUT
        let token = "your_token_here"  // Replace with the actual token
        
        resumenService.fetchReportItems(for: rut, token: token) { [weak self] items in
            DispatchQueue.main.async {
                self?.reportItems = items
                // Reload your table view or collection view here
                self?.tableView.reloadData()
            }
        }
    }
  

    func loadPage(page: Int) {
        let startIndex = (page - 1) * pageSize
        let endIndex = min(startIndex + pageSize, reportItems.count)
        if startIndex < endIndex {
            let newItems = Array(reportItems[startIndex..<endIndex])
            tableView.reloadData()
        }
        updateFooterView() // Actualizar el pie de la tabla
    }

      func configureTableHeader() {
          let headerTitles = ["No.", "Fecha Mov", "Tipo Mov", "Tipo Doc", "Cantidad de documentos", "Emisor/Librador/Arrendador", "Base de datos de publicación"]
          let headerWidths: [CGFloat] = [50, 100, 100, 100, 150, 150, 150]
          
          headerView.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50)
          headerView.backgroundColor = UIColor.lightGray
          
          var xPosition: CGFloat = 0
          for (index, title) in headerTitles.enumerated() {
              let label = UILabel()
              label.text = title
              label.font = UIFont.boldSystemFont(ofSize: 11)
              label.textAlignment = .center
              label.frame = CGRect(x: xPosition, y: 0, width: headerWidths[index], height: 50)
              headerView.addSubview(label)
              xPosition += headerWidths[index]
          }
          
          view.addSubview(headerView)
          headerView.translatesAutoresizingMaskIntoConstraints = false
          NSLayoutConstraint.activate([
              headerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
              headerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
              headerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
              headerView.heightAnchor.constraint(equalToConstant: 50)
          ])
      }
      
    func configureTableFooter() {
            footerView.backgroundColor = UIColor.lightGray
            footerView.translatesAutoresizingMaskIntoConstraints = false
            
            // Configurar los botones de paginación
            previousButton.translatesAutoresizingMaskIntoConstraints = false
            previousButton.setTitle("", for: .normal)
            previousButton.setTitleColor(.blue, for: .normal)
            previousButton.addTarget(self, action: #selector(previousButtonTapped), for: .touchUpInside)
            
            nextButton.translatesAutoresizingMaskIntoConstraints = false
            nextButton.setTitle("", for: .normal)
            nextButton.setTitleColor(.blue, for: .normal)
            nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
            
            // Configurar el botón de volver
            backButton.translatesAutoresizingMaskIntoConstraints = false
            backButton.setTitle("Volver", for: .normal)
            backButton.setTitleColor(.blue, for: .normal)
            backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
 
            footerView.addSubview(previousButton)
            footerView.addSubview(nextButton)
            footerView.addSubview(backButton)
            view.addSubview(footerView)
            
            NSLayoutConstraint.activate([
                footerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                footerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                footerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
                footerView.heightAnchor.constraint(equalToConstant: 60),
                
                backButton.leadingAnchor.constraint(equalTo: footerView.leadingAnchor, constant: 16),
                backButton.centerYAnchor.constraint(equalTo: footerView.centerYAnchor),
                
                previousButton.trailingAnchor.constraint(equalTo: nextButton.leadingAnchor, constant: -16),
                previousButton.centerYAnchor.constraint(equalTo: footerView.centerYAnchor),
                
                nextButton.trailingAnchor.constraint(equalTo: footerView.trailingAnchor, constant: -110),
                nextButton.centerYAnchor.constraint(equalTo: footerView.centerYAnchor),
                
            ])
        }
    

        func updateFooterView() {
            previousButton.isEnabled = currentPage > 1
            nextButton.isEnabled = currentPage < totalPages
        }

        func setupTableViewConstraints() {
            view.addSubview(tableView)
            NSLayoutConstraint.activate([
                tableView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
                tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                tableView.bottomAnchor.constraint(equalTo: footerView.topAnchor) // Ajuste aquí para no solapar el footer
            ])
        }
    
   @objc func backButtonTapped() {
       self.dismiss(animated: true, completion: nil)
   }
    @objc func downloadButtonTapped() {
        // Lógica para descargar el PDF
        print("Ver PDF")
    }
    @objc func pageButtonTapped(_ sender: UIButton) {
        currentPage = sender.tag
        loadPage(page: currentPage)
    }
    @objc func nextButtonTapped() {
        if currentPage < totalPages {
            currentPage += 1
            loadPage(page: currentPage)
        }
    }

    @objc func previousButtonTapped() {
        if currentPage > 1 {
            currentPage -= 1
            loadPage(page: currentPage)
        }
    }
    // MARK: - UITableViewDataSource Methods
       
    // Métodos de UITableViewDataSource y UITableViewDelegate
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           let startIndex = (currentPage - 1) * pageSize
           let endIndex = min(startIndex + pageSize, reportItems.count)
           return endIndex - startIndex
       }

       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomReportCell", for: indexPath) as? ReportTableViewCell else {
               return UITableViewCell()
           }
           let startIndex = (currentPage - 1) * pageSize
           let item = reportItems[startIndex + indexPath.row]
           cell.numeroLabel.text = "\(startIndex + indexPath.row + 1)"
           cell.fechaMovimientoLabel.text = item.fechaMovimiento
           cell.tipoMovimientoLabel.text = item.tipoMovimiento
           cell.tipoDocumentoLabel.text = item.tipoDocumento
           cell.cantidadDocumentosLabel.text = item.cantidadDocumentos
           cell.emisorLabel.text = item.emisor
           cell.baseDatosLabel.text = item.baseDatos
           
           // Configurar el tamaño de la fuente para las filas
           let fontSize: CGFloat = 12
           cell.numeroLabel.font = UIFont.systemFont(ofSize: fontSize)
           cell.fechaMovimientoLabel.font = UIFont.systemFont(ofSize: fontSize)
           cell.tipoMovimientoLabel.font = UIFont.systemFont(ofSize: fontSize)
           cell.tipoDocumentoLabel.font = UIFont.systemFont(ofSize: fontSize)
           cell.cantidadDocumentosLabel.font = UIFont.systemFont(ofSize: fontSize)
           cell.emisorLabel.font = UIFont.systemFont(ofSize: fontSize)
           cell.baseDatosLabel.font = UIFont.systemFont(ofSize: fontSize)
           
           // Imprime los valores en la consola para verificar
           print("Item \(indexPath.row):")
           print("Fecha de Movimiento: \(item.fechaMovimiento)")
           print("Tipo de Movimiento: \(item.tipoMovimiento)")
           print("Tipo de Documento: \(item.tipoDocumento)")
           print("Cantidad de Documentos: \(item.cantidadDocumentos)")
           print("Emisor: \(item.emisor)")
           print("Base de Datos: \(item.baseDatos)")
           
           return cell
       }
       
       // MARK: - UITableViewDelegate Methods
    

    @IBAction func backResumen() {
        self.dismiss(animated: true, completion: nil)
    }
    
     
    
}
