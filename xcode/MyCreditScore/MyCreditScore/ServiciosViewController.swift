import UIKit
import Foundation


class ServiciosViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalValue: UILabel!
    @IBOutlet weak var labelError: UILabel!
    
    var listaDeItems: [Item] = []
    
    var listaDeFavoritos: [Favorito] = []
    var footerLabel = UILabel()
    
    
    struct Favorito {
        var nombre: String
        var detalle: String
        // Otras propiedades relevantes
    }
    
    var selectedItemsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = .black
        label.numberOfLines = 1
        label.textAlignment = .right
        return label
    }()
    
    let cartButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "cart"), for: .normal)
        button.tintColor = .systemBlue
        button.translatesAutoresizingMaskIntoConstraints = false

        return button
    }()
    
        override func viewDidLoad() {
            super.viewDidLoad()
            //labelError.isHidden = true
            selectedItemsLabel.isHidden=true
            tableView.delegate = self
            tableView.dataSource = self
            tableView.allowsMultipleSelection = true
            tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: "CustomCell")
            fetchItems()
            
            // Setup constraints for tableView
            tableView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                // Assuming that totalValue is at the top and labelError is below it
                totalValue.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                totalValue.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                totalValue.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                
                labelError.topAnchor.constraint(equalTo: totalValue.bottomAnchor),
                labelError.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                labelError.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                
                tableView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                tableView.widthAnchor.constraint(equalToConstant: 400), // Ajusta el ancho según sea necesario
                tableView.heightAnchor.constraint(equalToConstant: 400) // Ajusta el alto según sea necesario
                
            ])
            
            // Agregar una cabecera a la tabla
            let headerView = UIView()
            headerView.backgroundColor = .systemGray6
            headerView.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 60)

            let headerLabel = UILabel()
            headerLabel.text = "Detalle"
            headerLabel.font = UIFont.boldSystemFont(ofSize: 20)
            headerLabel.translatesAutoresizingMaskIntoConstraints = false
            headerView.addSubview(headerLabel)
            
            headerView.addSubview(selectedItemsLabel)
            headerView.addSubview(cartButton)
            
            // Add constraints
            //headerLabel.translatesAutoresizingMaskIntoConstraints = false
            //cartButton.translatesAutoresizingMaskIntoConstraints = false
            //selectedItemsLabel.translatesAutoresizingMaskIntoConstraints = false

            NSLayoutConstraint.activate([
                headerLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16),
                headerLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),

                cartButton.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -16),
                cartButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
                cartButton.widthAnchor.constraint(equalToConstant: 100),
                cartButton.heightAnchor.constraint(equalToConstant: 30),

                selectedItemsLabel.trailingAnchor.constraint(equalTo: cartButton.leadingAnchor, constant: -8),
                selectedItemsLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor)
            ])
            
            tableView.tableHeaderView = headerView
            
            let footerView = UIView()
            footerView.backgroundColor = .systemGray6
            footerView.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 60)
 
            let totalSelected = calculateTotalSelected()
            footerLabel.text = "Total: \(totalSelected)"
            footerLabel.font = UIFont.boldSystemFont(ofSize: 20)
            footerLabel.translatesAutoresizingMaskIntoConstraints = false

            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.locale = Locale.current
            formatter.groupingSeparator = "."
            formatter.groupingSize = 3

            if let formattedValue = formatter.string(from: NSNumber(value: totalSelected)) {
                footerLabel.text = "Total: $\(formattedValue)"
            }
            
            footerView.addSubview(footerLabel)
               
            NSLayoutConstraint.activate([
                footerLabel.centerXAnchor.constraint(equalTo: footerView.centerXAnchor),
                footerLabel.topAnchor.constraint(equalTo: footerView.topAnchor, constant: 16),
                
            ])
                   
            tableView.tableFooterView = footerView
            updateSelectedItemsLabel()
           
        }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44 // Altura deseada para las celdas de la tabla
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as? CustomTableViewCell else {
            return UITableViewCell()
        }
        
        let item = listaDeItems[indexPath.row]
        cell.nameLabel.text = item.nombre
        cell.checkboxButton.isSelected = item.isSelected
        cell.checkboxButton.addTarget(self, action: #selector(checkboxTapped(_:)), for: .touchUpInside)
        cell.iconImageView.image = UIImage(named: item.iconName)
        cell.favoriteButton.isSelected = listaDeItems[indexPath.row].isSelected  //isFavorite
        cell.favoriteButton.addTarget(self, action: #selector(favoriteButtonTapped(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = listaDeItems[indexPath.row]
        selectedItem.isSelected.toggle()

        if selectedItem.isSelected {
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        } else {
            tableView.deselectRow(at: indexPath, animated: true)
        }

        if selectedItem.isSelected {
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.locale = Locale.current
            formatter.groupingSeparator = "."
            formatter.groupingSize = 3
            
            if let formattedValue = formatter.string(from: NSNumber(value: selectedItem.totalValue)) {
                totalValue.text = "Detalle Precio $\(formattedValue)"
            } else {
                totalValue.text = ""
            }
        } else {
            totalValue.text = ""
        }
        updateFooterTotal()
        updateSelectedItemsLabel()
        saveSelectedItemsToSession()
    }

  
    
    func fetchItems() {
            //guard let url = URL(string: "http://localhost:3001/api/items") else { return }
            let baseURL = Config.apiEndpoint // Asegúrate de que esta línea esté aquí
            guard let url = URL(string: "\(baseURL)/api/items") else { return }

            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.addValue("your-api-key", forHTTPHeaderField: "ApiKey")
            request.addValue("your-client-id", forHTTPHeaderField: "clientID")

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error {
                    self.mostrarError(mensaje: "Error al conectar con el servidor: \(error.localizedDescription)")
                    print("Error fetching items: \(error)")
                    return
                }

                guard let data = data else {
                    self.mostrarError(mensaje: "Datos inválidos recibidos del servidor")
                    print("No data received")
                    return
                }

                do {
                    let items = try JSONDecoder().decode([Item].self, from: data)
                    DispatchQueue.main.async {
                        self.listaDeItems = items
                        self.tableView.reloadData()
                        
                        // Guardar ítems seleccionados en la sesión después de cargar los datos
                        self.saveSelectedItemsToSession()
                    }
                } catch {
                    self.mostrarError(mensaje: "Error al decodificar los elementos: \(error.localizedDescription)")
                    print("Error decoding items: \(error)")
                }
            }

            task.resume()
        }
    


    private func showError(message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func saveItemsToSession(items: [Item]) {
        let encoder = JSONEncoder()
        do {
            let encodedData = try encoder.encode(items)
            UserDefaults.standard.set(encodedData, forKey: "savedItems")
            mostrarMensajeExito(mensaje: "Items saved successfully.")
        } catch {
            mostrarError(mensaje: "Failed to encode items: \(error.localizedDescription)")
        }
    }

    func findItemByName(_ name: String) -> Item? {
        return listaDeItems.first(where: { $0.nombre == name })
    }

    
    func loadItemsFromSession() -> [Item]? {
        var savedItems: [Item]?
        if let savedData = UserDefaults.standard.data(forKey: "savedItems") {
            let decoder = JSONDecoder()
            do {
                savedItems = try decoder.decode([Item].self, from: savedData)
            } catch {
                print("Error decoding items: \(error.localizedDescription)")
            }
        }
        return savedItems
    }


    //save data in sesion
    func saveSelectedItemsToSession() {
        let selectedItems = listaDeItems.filter { $0.isSelected }
        print("Elementos seleccionados:")
        for item in selectedItems {
            print("- Nombre: \(item.nombre), Precio: \(item.price ?? 0), Total Value: \(item.totalValue)")
        }
        saveItemsToSession(items: selectedItems)
    }

    
    @objc func checkboxTapped(_ sender: UIButton) {
        if let cell = sender.superview?.superview as? CustomTableViewCell,
           let indexPath = tableView.indexPath(for: cell) {
            listaDeItems[indexPath.row].isSelected.toggle()
            sender.isSelected = listaDeItems[indexPath.row].isSelected
            updateFooterTotal()
            updateSelectedItemsLabel()
            saveSelectedItemsToSession()
        }
    }

    // Función para actualizar el número de elementos seleccionados en la etiqueta
    func updateSelectedItemsLabel() {
        let totalSelectedItems = listaDeItems.filter { $0.isSelected }.count
        selectedItemsLabel.text = "\(totalSelectedItems)"
        cartButton.setTitle("\(totalSelectedItems)", for: .normal)
    }
    
    func updateFooterTotal() {
        let totalSelected = calculateTotalSelected()

        if totalSelected == 0 {
            // Si no hay ningún checkButton seleccionado, actualiza el total a cero
            if let footerView = tableView.tableFooterView,
               let footerLabel = footerView.subviews.first as? UILabel {
                footerLabel.text = "Total: $0"
            }
        } else {
            // Si hay checkButtons seleccionados, actualiza el total normalmente
            if let footerView = tableView.tableFooterView,
               let footerLabel = footerView.subviews.first as? UILabel {
                let formatter = NumberFormatter()
                formatter.numberStyle = .decimal
                formatter.locale = Locale.current
                formatter.groupingSeparator = "."
                formatter.groupingSize = 3

                if let formattedValue = formatter.string(from: NSNumber(value: totalSelected)) {
                    footerLabel.text = "Total: $\(formattedValue)"
                }
            }
        }
    }

    
    func calculateTotalSelected() -> Double {
        return listaDeItems.filter { $0.isSelected }.reduce(0) { result, item in
            if item.isSelected {
                return result + item.totalValue
            } else {
                return result
            }
        }
    }

    
    @objc func continueWithPurchase() {
            // Acción a realizar al pulsar el botón "Continuar con la compra"
            //let total = calculateTotalSelected()
            //let alert = UIAlertController(title: "Compra", message: "Total a pagar: \(total)", preferredStyle: .alert)
            //alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            //present(alert, animated: true, completion: nil)
            saveSelectedItemsToSession()
        // Acción configurada en el Storyboard
            performSegue(withIdentifier: "CarritoViewController", sender: self)

        }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CarritoViewController" {
            if let carritoVC = segue.destination as? CarritoViewController {
                // Cargar los datos guardados en sesión
                let savedItems = loadItemsFromSession()
                if let items = savedItems, !items.isEmpty {
                    carritoVC.cartItems = savedItems ?? []
                    } else {
                        print("No hay elementos guardados.")
                        mostrarError(mensaje: "No hay elementos guardados.")
                        carritoVC.errorMessage = "No hay elementos guardados."
                    }
                } else {
                    print("Error al cargar elementos guardados.")
                    mostrarError(mensaje: "Error al cargar elementos guardados.")
                }
            }
        }
    

  
    func extractTotal(from label: UILabel) -> Double? {
        guard let totalText = label.text?.replacingOccurrences(of: "$", with: "") else {
            return nil
        }
        return Double(totalText)
    }
    @IBAction func favoriteButtonTapped(_ sender: UIButton) {
        guard let cell = sender.superview?.superview as? CustomTableViewCell,
                  let index = tableView.indexPath(for: cell) else {
                return
            }
        sender.isSelected.toggle()

        if sender.isSelected {
            sender.tintColor = UIColor.red
            // Mostrar el modal para ingresar el nombre de la lista y guardar el favorito
            showAddFavoriteModal(for: index.row)
        } else {
            sender.tintColor = UIColor.systemBlue
        }
    }

    func showAddFavoriteModal(for itemIndex: Int) {
        // Verificar si ya existe una lista guardada
        if let existingList = !listaDeFavoritos.isEmpty ? obtenerListaFavoritos(nombre: String(itemIndex)) : nil {
            // Si la lista ya existe, guardar el favorito directamente
            guardarFavorito(nombre: String(itemIndex), detalle: existingList)
            return
        }

        let alertController = UIAlertController(title: "Guardar Favorito", message: "Ingrese el nombre de la lista", preferredStyle: .alert)
        alertController.addTextField { textField in
            textField.placeholder = "Nombre de la lista"
        }
        let saveAction = UIAlertAction(title: "Guardar", style: .default) { _ in
            if let name = alertController.textFields?.first?.text {
                // Guardar el favorito con el nombre del ítem y el nombre de la lista
                self.guardarFavorito(nombre: String(itemIndex), detalle: name)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)

        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)

        present(alertController, animated: true, completion: nil)
    }


    func obtenerListaFavoritos(nombre: String) -> String? {
        // Verificar si la lista de favoritos no está vacía
        if !listaDeFavoritos.isEmpty {
            // Retornar el primer elemento de la lista
            return listaDeFavoritos.first?.nombre
        } else {
            return nil
        }
    }

    
    func guardarFavorito(nombre: String, detalle: String) {
        let favorito = Favorito(nombre: nombre, detalle: detalle)
        listaDeFavoritos.append(favorito)
        // Aquí puedes guardar la lista de favoritos en un almacenamiento persistente si es necesario
    }

    func mostrarError(mensaje: String) {
          DispatchQueue.main.async {
              self.labelError.isHidden = false // Mostrar el labelError
              self.labelError.textColor = .red
              self.labelError.text = mensaje
          }
      }
    
    func mostrarMensajeExito(mensaje: String) {
        DispatchQueue.main.async {
            self.labelError.isHidden = false
            self.labelError.textColor = .green
            self.labelError.text = "Mensaje: $$mensaje) Service successful"
        }
    }
    
    @IBAction func volver(_ sender: Any) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func backResumen() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
