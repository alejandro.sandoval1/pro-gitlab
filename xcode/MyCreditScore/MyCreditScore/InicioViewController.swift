import UIKit
import SwiftUI

class InicioViewController: UIViewController {

    @IBOutlet weak var revisarButton: UIButton!
    private var hostingController: UIHostingController<SnapCarouselWrapper>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configura el carousel
        let snapCarouselWrapper = SnapCarouselWrapper()
        hostingController = UIHostingController(rootView: snapCarouselWrapper)
        if let hostingController = hostingController {
            hostingController.view.backgroundColor = .white
            addChild(hostingController)
            view.addSubview(hostingController.view)
            hostingController.didMove(toParent: self)
            
            // Configura el frame del carousel
            hostingController.view.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                hostingController.view.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                  hostingController.view.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -50), // Ajuste de la posición vertical
                  hostingController.view.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.9),
                  hostingController.view.heightAnchor.constraint(equalToConstant: 300),
                
                // Configura el botón revisarButton
                //revisarButton.topAnchor.constraint(equalTo: hostingController.view.bottomAnchor, constant: 20),
                //revisarButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
                //revisarButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
            ])
        }
    }
}
