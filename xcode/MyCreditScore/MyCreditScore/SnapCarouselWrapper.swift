import SwiftUI

struct CarouselItem {
    var imageName: String
    var label: String
    var description: String
}

struct SnapCarouselWrapper: UIViewControllerRepresentable {
    func makeUIViewController(context: Context) -> UIViewController {
        let viewController = UIHostingController(rootView: SnapCarouse())
        viewController.view.backgroundColor = .white // Establece el fondo en blanco
        return viewController
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
        // No necesitas implementar esto a menos que quieras realizar alguna actualización específica
    }
}

struct SnapCarouse: View {
    var body: some View {
        NavigationView {
            AutoScroller(
                items: [
                    CarouselItem(imageName: "carousel1", label: "Informe Comercial", description: "Detalles del informe comercial."),
                    CarouselItem(imageName: "carousel2", label: "Reporte Anual", description: "Información sobre el reporte anual."),
                    CarouselItem(imageName: "carousel3", label: "Resumen Financiero", description: "Resumen financiero del año.")
                ]
            )
            .background(Color.white) // Establece el fondo en blanco
        }
    }
}

struct SnapCarouse_Previews: PreviewProvider {
    static var previews: some View {
        SnapCarouse()
    }
}

struct AutoScroller: View {
    var items: [CarouselItem]
    let timer = Timer.publish(every: 2.5, on: .main, in: .common).autoconnect()

    @State private var selectedImageIndex: Int = 0

    var body: some View {
        ZStack {
            Color.white // Establece el fondo en blanco
                .ignoresSafeArea()

            TabView(selection: $selectedImageIndex) {
                ForEach(0..<items.count, id: \.self) { index in
                    NavigationLink(destination: DestinationView(item: items[index])) {
                        ZStack(alignment: .bottom) {
                            Image(items[index].imageName)
                                .resizable()
                                .tag(index)
                                .frame(width: 350, height: 200)
                            Text(items[index].label)
                                .font(.headline)
                                .padding(.bottom, 10)
                                .background(Color.black.opacity(0.6))
                                .foregroundColor(.white)
                                .cornerRadius(5)
                                .padding(.horizontal, 10)
                        }
                        .frame(width: 350, height: 200)
                        .shadow(radius: 20)
                    }
                }
            }
            .frame(height: 300)
            .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
            .ignoresSafeArea()

            HStack {
                ForEach(0..<items.count, id: \.self) { index in
                    Capsule()
                        .fill(Color.white.opacity(selectedImageIndex == index ? 1 : 0.33))
                        .frame(width: 35, height: 8)
                        .onTapGesture {
                            selectedImageIndex = index
                        }
                }
                .offset(y: 130)
            }
        }
        .onReceive(timer) { _ in
            withAnimation(.default) {
                selectedImageIndex = (selectedImageIndex + 1) % items.count
            }
        }
    }
}

struct DestinationView: View {
    var item: CarouselItem

    var body: some View {
        VStack(alignment: .leading) {
            Text(item.description)
                .font(.body)
                .padding()
            if item.label == "Informe Comercial" {
                VStack(alignment: .leading, spacing: 10) {
                    Text("Accede al Informe:")
                        .font(.headline)
                        .padding(.top)
                    Text("• Conoce las deudas morosas publicadas para tu RUT.")
                        .multilineTextAlignment(.leading) // Ajusta la justificación del texto
                        .padding(.leading)
                        .foregroundColor(.black)
                        .font(.footnote) // Cambia el tamaño de la letra a footnote
                    Text("• Visualiza bienes raíces y vehículos registrados.")
                        .multilineTextAlignment(.leading) // Ajusta la justificación del texto
                        .padding(.leading)
                        .foregroundColor(.black)
                        .font(.footnote) // Cambia el tamaño de la letra a footnote
                    Text("• Conoce tu puntaje con el indicador de riesgo asociado a tu RUT.")
                        .multilineTextAlignment(.leading) // Ajusta la justificación del texto
                        .padding(.leading)
                        .foregroundColor(.black)
                        .font(.footnote) // Cambia el tamaño de la letra a footnote
                    Text("• Revisa antecedentes personales como fecha de nacimiento, estado civil, direcciones, entre otros.")
                        .multilineTextAlignment(.leading) // Ajusta la justificación del texto
                        .padding(.leading)
                        .foregroundColor(.black)
                        .font(.footnote) // Cambia el tamaño de la letra a footnote
                }
                .padding()
            } else if item.label == "Resumen Financiero" {
                VStack(alignment: .leading, spacing: 15) {
                   // Text("Detalles del Resumen Financiero:")
                   //     .font(.headline)
                   //     .padding(.top)
                    Text("Este registro permite ver deudas morosas, protestos,   infracciones laborales, entre otros, asociados a un RUT.")
                        .padding(.leading)
                        .foregroundColor(.black)
                        .font(.footnote)
                    Text("Sus fuentes de datos son:")
                        .font(.headline)
                        .padding(.top)
                    Text("• Deudas publicadas en el Boletín Electrónico Dicom.")
                        .padding(.leading)
                        .foregroundColor(.black)
                        .font(.footnote)
                    Text("• Deudas de grandes tiendas.")
                        .padding(.leading)
                        .foregroundColor(.black)
                        .font(.footnote)
                    Text("• Protestos de la Cámara de Comercio.")
                        .padding(.leading)
                        .foregroundColor(.black)
                        .font(.footnote)
                    Text("• Deudas laborales de la Dirección del Trabajo.")
                        .padding(.leading)
                        .foregroundColor(.black)
                        .font(.footnote)
                }
                .padding()
            }
        }
        .navigationTitle(item.label)
    }
}
