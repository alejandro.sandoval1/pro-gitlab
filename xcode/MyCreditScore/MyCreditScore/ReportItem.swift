//
//  ReportItem.swift
//  MyCreditScore
//
//  Created by Monica Araneda on 20-06-24.
//

import Foundation

struct ReportItem: Codable {
    var fechaMovimiento: String
    var tipoMovimiento: String
    var tipoDocumento: String
    var cantidadDocumentos: String
    var emisor: String
    var baseDatos: String
}
