//
//  RegisterViewController.swift
//  MyCreditScore
//
//  Created by Alejandro Schwartz on 02-06-24.
//

import UIKit


struct User: Codable {
    let nombre: String
    let apellido: String
    let rut: String
    let numeroSerie: String
}


class RegisterViewController: UIViewController {
    
    @IBOutlet weak var nombre: UITextField!
    @IBOutlet weak var apellido: UITextField!
    @IBOutlet weak var rut: UITextField!
    @IBOutlet weak var numdocumento: UITextField!
    @IBOutlet weak var errorMensaje: UILabel!
    @IBOutlet weak var info: UIButton!
    @IBOutlet weak var aceptar: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorMensaje.isEnabled=true
        
        // Configure the existing segmented control
        let items = ["1", "2", "3", "4"]
        segmentedControl.removeAllSegments()
        for (index, item) in items.enumerated() {
            segmentedControl.insertSegment(withTitle: item, at: index, animated: false)
        }
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.isUserInteractionEnabled = false
        
        updateSegmentedControl(step: 1)
        // Do any additional setup after loading the view.
    }
    
    
    func updateSegmentedControl(step: Int) {
          for index in 0..<segmentedControl.numberOfSegments {
              segmentedControl.setEnabled(false, forSegmentAt: index)
          }
          segmentedControl.setEnabled(true, forSegmentAt: step - 1)
          segmentedControl.selectedSegmentIndex = step - 1
      }
    
    
    @IBAction func registrarUsuario(_ sender: Any) {
        guard let nombre = nombre.text,
              let apellido = apellido.text,
              let rut = rut.text,
              let numeroSerie = numdocumento.text else {
            // Handle error, e.g., display an alert
            return
        }
            
            let user = User(nombre: nombre, apellido: apellido, rut: rut, numeroSerie: numeroSerie)
            
            registerUser(user: user) { (error) in
                if let error = error {
                    // Handle error, e.g., display an alert
                    self.mostrarError(mensaje:"Error al registrar usuario: \(error)")
                } else {
                    // Registro exitoso, puedes mostrar un mensaje de éxito
                    self.mostrarError(mensaje:"Usuario registrado exitosamente")
                }
            }
        }
        
    func registerUser(user: User, completion: @escaping (Error?) -> Void) {
        guard let url = URL(string: "https://localhost/api/register") else {
            completion(NSError(domain: "TuApp", code: -1, userInfo: [NSLocalizedDescriptionKey: "URL inválida"]))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            let jsonData = try JSONEncoder().encode(user)
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    completion(error)
                    return
                }
                
                // Verificar la respuesta del servidor
                guard let httpResponse = response as? HTTPURLResponse else {
                    completion(NSError(domain: "TuApp", code: -1, userInfo: [NSLocalizedDescriptionKey: "Respuesta inválida"]))
                    return
                }
                
                if httpResponse.statusCode == 200 {
                    // El usuario se registró correctamente
                    completion(nil)
                } else {
                    // El servidor devolvió un código de estado diferente a 200
                    completion(NSError(domain: "TuApp", code: httpResponse.statusCode, userInfo: [NSLocalizedDescriptionKey: "Error de registro"]))
                }
            }
            
            task.resume()
        } catch {
            completion(error)
        }
    }

    @IBAction func mostrarInfo(_ sender: Any) {
            // Crear el UIAlertController
            let alert = UIAlertController(title: "", message: nil, preferredStyle: .alert)
            
            // Crear la vista personalizada
            let customView = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 300))
            
            // Crear el UISegmentedControl
            let segmentedControl = UISegmentedControl(items: ["Cédula Actual", "Cédula Antigua"])
            segmentedControl.frame = CGRect(x: 10, y: 10, width: 230, height: 30)
            segmentedControl.selectedSegmentIndex = 0
            customView.addSubview(segmentedControl)
            
            // Crear las UIImageViews
            let imageViewActual = UIImageView(frame: CGRect(x: 10, y: 50, width: 230, height: 230))
            imageViewActual.image = UIImage(named: "cedulaactual.png")
            customView.addSubview(imageViewActual)
            
            let imageViewAntigua = UIImageView(frame: CGRect(x: 10, y: 50, width: 230, height: 230))
            imageViewAntigua.image = UIImage(named: "cedulaantigua.png")
            imageViewAntigua.isHidden = true
            customView.addSubview(imageViewAntigua)
            
            // Manejar el cambio de segmento
            segmentedControl.addTarget(self, action: #selector(segmentChanged(_:)), for: .valueChanged)
            
            // Agregar la vista personalizada al UIAlertController
            alert.view.addSubview(customView)
            
            // Ajustar el tamaño del UIAlertController
            let height = NSLayoutConstraint(item: alert.view!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 370)
            alert.view.addConstraint(height)
            
            // Agregar la acción de cerrar
            alert.addAction(UIAlertAction(title: "Cerrar", style: .default, handler: nil))
            
            // Presentar el UIAlertController
            self.present(alert, animated: true, completion: nil)
        }
        
        @objc func segmentChanged(_ sender: UISegmentedControl) {
            // Obtener la vista personalizada
            let customView = sender.superview!
            let imageViewActual = customView.subviews[1] as! UIImageView
            let imageViewAntigua = customView.subviews[2] as! UIImageView
            
            // Mostrar u ocultar las imágenes según el segmento seleccionado
            if sender.selectedSegmentIndex == 0 {
                imageViewActual.isHidden = false
                imageViewAntigua.isHidden = true
            } else {
                imageViewActual.isHidden = true
                imageViewAntigua.isHidden = false
            }
        }
    
    
    @IBAction func nexStepButton(_ sender: UIButton) {
        print("El botón 'aceptar' ha sido presionado")
        self.performSegue(withIdentifier: "RegisterStep2ViewController", sender: self)
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if segue.identifier == "RegisterStep2ViewController" {
           if segue.destination is RegisterStep2ViewController {
               // Pasar el monto total a MediosViewController
          
           }
       }
    }

    
    func mostrarError(mensaje: String) {
        DispatchQueue.main.async {
            self.errorMensaje.isHidden = false
            self.errorMensaje.textColor = .red
            self.errorMensaje.text = mensaje
        }
    }
    
    @IBAction func volver(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func Cancelar(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
