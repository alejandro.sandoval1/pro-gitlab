//
//  LoadingViewController.swift
//  MyCreditScore
//
//  Created by Alejandro Schwartz on 25-05-24.
//

import UIKit

class LoadingViewController: UIViewController {

   
    @IBOutlet weak var activityIndicators: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicators.startAnimating()
    }

}
