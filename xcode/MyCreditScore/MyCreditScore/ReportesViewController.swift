//
//  ReportesViewController.swift
//  MyCreditScore
//
//  Created by Alejandro Schwartz on 25-05-24.
//

import UIKit

class ReportesViewController: UIViewController {
    
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var creditScoreLabel: UILabel!
    @IBOutlet weak var creditStatusLabel: UILabel!
    
    @IBOutlet weak var utilizationLabel: UILabel!
    @IBOutlet weak var paymentHistoryLabel: UILabel!
    @IBOutlet weak var totalAccountsLabel: UILabel!
    @IBOutlet weak var hardInquiriesLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var balanceProgressView: UIProgressView!
    
    @IBOutlet weak var arcView: ArcView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var contentView: UIView!
 
    
    let resumenService = ResumenService()
    var resumenData: ResumenData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.isHidden = true
        //showResumenContent()
        
        // Configurar el segmented control
        configureSegmentedControl()
        
        fetchResumenData()
        
        // Configurar la barra de progreso del balance
        balanceProgressView.progress = 0.55 // Ejemplo de progreso
        balanceProgressView.tintColor = .green

    }
    
    private func fetchResumenData() {
        resumenService.fetchResumen(for: "12345678-9", token: "your-token") { [weak self] resumenData in
            guard let self = self else { return }

            DispatchQueue.main.async {
                if let resumenData = resumenData {
                    self.resumenData = resumenData
                    self.updateUI(with: resumenData)
                } else {
                    self.showError()
                }
            }
        }
    }
    
    // En Interface Builder:
    // 1. Selecciona cada UILabel.
    // 2. Ve al inspector de atributos.
    // 3. Asegúrate de que la propiedad "Lines" está establecida en 0 (lo que permite múltiples líneas).
    // 4. Ajusta la propiedad "Line Break" a "Word Wrap" para que el texto se ajuste automáticamente.
    private func updateUI(with data: ResumenData) {
        print("updateUI")
        let formattedUBalanceTotalAmount = formatCurrency(data.totalBalance)
         
        // Imprimir todos los valores de data
        print("Nombre: \(data.nombre)")
        print("Credit Score: \(data.creditScore)")
        print("Credit Status: \(data.creditStatus)")
        print("Utilización: \(data.utilization)")
        print("Historial de Pagos: \(data.paymentHistory)")
        print("Total de Cuentas: \(data.totalAccounts)")
        print("Consultas Difíciles: \(data.hardInquiries)")
        print("Balance Total: \(formattedUBalanceTotalAmount)")
        
        let bienvenida = data.gender == "F" ? "Bienvenida" : "Bienvenido"
        let fullWelcomeText = "\(bienvenida) \(data.nombre)"
        let attributedWelcomeText = NSMutableAttributedString(string: fullWelcomeText)
        let boldRange = (fullWelcomeText as NSString).range(of: bienvenida)
        attributedWelcomeText.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: welcomeLabel.font.pointSize), range: boldRange)
        welcomeLabel.attributedText = attributedWelcomeText
        creditScoreLabel.text = "\(data.creditScore)"
        creditStatusLabel.text = "\(data.creditStatus)"
        utilizationLabel.text = "\(data.utilization)% \nUtilización"
        paymentHistoryLabel.text = "\(data.paymentHistory)% \nHistorial de Pagos"
        totalAccountsLabel.text = "\(data.totalAccounts) \nTotal de Cuentas\n"
        hardInquiriesLabel.text = "\(data.hardInquiries) \nConsultas Difíciles"
        
        // Configurar balanceLabel con texto personalizado y estilo
        let balanceText = "Actualizaciones de su informe de crédito\nCuenta\nCómo se ve su cuenta hoy\nTotal Balance                                         $\(formattedUBalanceTotalAmount)"
        let attributedBalanceText = NSMutableAttributedString(string: balanceText)
        let boldBalanceRange = (balanceText as NSString).range(of: "Cuenta")
        attributedBalanceText.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: balanceLabel.font.pointSize + 2), range: boldBalanceRange)
        balanceLabel.attributedText = attributedBalanceText
        
        // Configurar la barra de progreso del balance
        let balanceProgress = data.totalBalance / 100000.0 // Ajusta el valor según tu lógica de progreso
        balanceProgressView.progress = Float(balanceProgress)
        balanceProgressView.tintColor = .green
        let creditScore = Int(data.creditScore)

        // Configurar arcView
          // arcView.creditScoreLabel.text = "\(data.creditScore)"
          // arcView.creditStatusLabel.text = "850"
           arcView.score = CGFloat(data.creditScore)
           arcView.maxScore = CGFloat(850) // Convertir 850 a CGFloat
           arcView.greenColor = .green
           arcView.redColor = .red
        
        // Configurar arcView
        if arcView.superview == nil {
            view.addSubview(arcView) // Agregar arcView a la vista principal en lugar del contentView
        }
        
        arcView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            arcView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 54),
            arcView.topAnchor.constraint(equalTo: view.topAnchor, constant: 175),
            arcView.widthAnchor.constraint(equalToConstant: 284),
            arcView.heightAnchor.constraint(equalToConstant: 150)
        ])
    }




    func configureSegmentedControl() {
         segmentedControl.removeAllSegments()
         segmentedControl.insertSegment(withTitle: "Resumen", at: 0, animated: false)
         segmentedControl.insertSegment(withTitle: "Creditos", at: 1, animated: false)
         segmentedControl.insertSegment(withTitle: "Morosidad", at: 2, animated: false)
         segmentedControl.selectedSegmentIndex = 0
         
         let blueTextAttributes: [NSAttributedString.Key: Any] = [
             .foregroundColor: UIColor.blue
         ]
         
         segmentedControl.setTitleTextAttributes(blueTextAttributes, for: .normal)
         segmentedControl.setTitleTextAttributes(blueTextAttributes, for: .selected)
         
         segmentedControl.addTarget(self, action: #selector(segmentChanged(_:)), for: .valueChanged)
     }
    
    @objc func segmentChanged(_ sender: UISegmentedControl) {
         guard let resumenData = resumenData else { return }
         updateContent(for: sender.selectedSegmentIndex, with: resumenData)
     }
       
    func updateContent(for index: Int, with resumenData: ResumenData) {
        // Eliminar todas las subviews actuales en contentView
        contentView.subviews.forEach { $0.removeFromSuperview() }
        
        switch index {
        case 0:
            showResumenContent()
        case 1:
            showCreditosContent(with: resumenData)
        case 2:
            showPasivosContent(with: resumenData)
        default:
            break
        }
    }
    

    func showResumenContent() {
        contentView.isHidden = true
         
     }
    
    func showCreditosContent(with data: ResumenData) {
        contentView.isHidden = false
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = "INFORME DE CREDITOS"
        titleLabel.textColor = .blue
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        titleLabel.textAlignment = .center
        
        let dateLabel = UILabel()
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.text = getCurrentDateAndTime()
        dateLabel.font = UIFont.systemFont(ofSize: 14)
        dateLabel.textAlignment = .center
        
        let messageLabel = UILabel()
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.text = generateCreditSummaryMessage(with: data)
        messageLabel.font = UIFont.systemFont(ofSize: 14)
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .left
        
        let stackView = UIStackView(arrangedSubviews: [titleLabel, dateLabel, messageLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.alignment = .center
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            stackView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -16)

        ])
    }
    
    /// <#Description#>
    /// - Parameter resumenData: <#resumenData description#>
    func showPasivosContent(with data: ResumenData) {
        contentView.isHidden = false
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = "INFORME DE MOROSIDAD"
        titleLabel.textColor = .red
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        titleLabel.textAlignment = .center
        
        let dateLabel = UILabel()
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.text = getCurrentDateAndTime()
        dateLabel.font = UIFont.systemFont(ofSize: 14)
        dateLabel.textAlignment = .center
        
        let messageLabel = UILabel()
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.text = generateMoroseSummaryMessage(with: data)
        messageLabel.font = UIFont.systemFont(ofSize: 14)
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .left
         
        let stackView = UIStackView(arrangedSubviews: [titleLabel, dateLabel, messageLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.alignment = .center
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            stackView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -16)

        ])
    }
        
    @objc func viewDetailTapped() {
        // Acción para el botón "ver detalle"
        print("Detalle del informe de deudas")
    }
    func configureArcView(score: CGFloat, maxScore: CGFloat, greenColor: UIColor, redColor: UIColor) {
            arcView.score = score
            arcView.maxScore = maxScore
            arcView.greenColor = greenColor
            arcView.redColor = redColor
        }
    func getCurrentDateAndTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_CL")
        dateFormatter.dateFormat = "dd 'de' MMMM yyyy 'a las' HH:mm 'hrs'"
        let currentDate = Date()
        return dateFormatter.string(from: currentDate)
    }
    func generateMoroseSummaryMessage(with resumenData: ResumenData) -> String {
        // Crear el saludo basado en el género del usuario
        let saludo = resumenData.gender == "F" ? "Estimada" : "Estimado"
        
        // Crear el mensaje basado en la información de morosidad
        var message = "\(saludo) \(resumenData.nombre),\n\n"
        message += "Detalles de Morosidad:\n"
        message += "- Estado del último deudor: \(resumenData.moroseSummary.lastDebtorStatus)\n"
        message += "- Total de documentos: \(resumenData.moroseSummary.totalNumberOfDocs)\n"
        message += "- Número de documentos morosos: \(resumenData.moroseSummary.moroseNumberOfDocs)\n"
        message += "- Monto total de morosidad: $\(resumenData.moroseSummary.moroseTotalAmount)\n"
        message += "- Número de consultas: \(resumenData.moroseSummary.numberInquiries)\n"
        message += "- Último período: \(resumenData.moroseSummary.lastPeriod)"
        
        print(message)
        return message
    }

    func generateCreditSummaryMessage(with resumenData: ResumenData) -> String {
        // Crear el saludo basado en el género del usuario
        let saludo = resumenData.gender == "F" ? "Estimada" : "Estimado"
        
        // Crear el mensaje basado en la información de los créditos
        var message = "\(saludo) \(resumenData.nombre),\n\n"
        
        if resumenData.creditData.isSubjectToCredit {
            message += "Nos complace informarle que está sujeto/a a crédito. Actualmente, usted tiene \(resumenData.creditData.totalCredits) crédito(s) activo(s)."
        } else {
            message += "Lo sentimos, pero no hemos encontrado información que indique que está sujeto/a a crédito en este momento."
        }
        let formattedUnpaidTotalAmount = formatCurrency(resumenData.unpaidTotalAmount)
          
        // Añadir detalles adicionales desde ResumenData
        message += "\n\nDetalles adicionales:\n"
        message += "- Monto total impago: $\(formattedUnpaidTotalAmount)\n"
        message += "- Total de documentos ICOM: \(resumenData.totalDocsICOM)\n"
        message += "- Total de documentos en los últimos 6 meses: \(resumenData.totalDocsLast6Months)"
        
        print(message)
        return message
    }

    func formatCurrency(_ amount: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        //formatter.currencySymbol = "$"
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.locale = Locale(identifier: "es_CL")
        return formatter.string(from: NSNumber(value: amount)) ?? "0.00"
    }

    private func showError() {
        balanceLabel.text = "Error al recuperar los datos. Por favor, inténtelo de nuevo."
        balanceLabel.textColor = .red
        balanceLabel.isHidden = false
        balanceLabel.backgroundColor = .white
    }
    
    @IBAction func volver() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
