//
//  MediosViewController.swift
//  MyCreditScore
//
//  Created by Alejandro Schwartz on 25-05-24.
//

import UIKit

class MediosViewController: UIViewController {

  
    @IBOutlet weak var cartTotal: UITextField!
    @IBOutlet weak var btnButton: UIButton!
    
    
    var totalAmount: String = "0.0"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Total Amount: \(totalAmount)")
        cartTotal.text = totalAmount
        addPaymentMethodButtons()
        btnButton.isEnabled = false

        // Personalizar el TextField
        cartTotal.borderStyle = .none
        cartTotal.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
        cartTotal.layer.cornerRadius = 5.0
        cartTotal.layer.borderWidth = 1.0
        cartTotal.layer.borderColor = UIColor.lightGray.cgColor
        cartTotal.textColor = UIColor.black
        cartTotal.font = UIFont.systemFont(ofSize: 16.0)
        cartTotal.textAlignment = .center
        
        // Deshabilitar la interacción del usuario para que sea de solo lectura
        cartTotal.isUserInteractionEnabled = false
    }
    
    
    func addPaymentMethodButtons() {
        let buttonWidth: CGFloat = 200
        let buttonHeight: CGFloat = 50
        let buttonSpacing: CGFloat = 10
        let yPosition: CGFloat = 400.0
        let xCenter: CGFloat = view.frame.width / 2
        
        let paymentMethods = ["Tarjeta de Crédito", "Tarjeta de Débito", "Escaneando QR", "WebPay"]
        
        for (index, method) in paymentMethods.enumerated() {
            let button = UIButton(type: .system)
            button.frame = CGRect(x: xCenter - buttonWidth / 2, y: yPosition + CGFloat(index) * (buttonHeight + buttonSpacing), width: buttonWidth, height: buttonHeight)
            button.setTitle(method, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 17)
            button.addTarget(self, action: #selector(paymentMethodSelected(_:)), for: .touchUpInside)
            button.tag = index // Asigna un tag para identificar el método de pago seleccionado
            button.layer.cornerRadius = buttonHeight / 2 // Hace que los botones tengan esquinas redondeadas
            button.backgroundColor = UIColor(red: 0.2, green: 0.6, blue: 0.8, alpha: 1.0) // Color de fondo azul claro
            button.setTitleColor(.white, for: .normal) // Texto blanco
            
            if method == "WebPay" {
                button.isEnabled = false // Deshabilitar el botón WebPay
                button.backgroundColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0) // Cambiar el color de fondo a gris
                button.setTitleColor(.darkGray, for: .normal) // Cambiar el color del texto a gris oscuro
            }
            if method == "Escaneando QR"{
                button.isEnabled = false // Deshabilitar el botón WebPay
                button.backgroundColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0) // Cambiar el color de fondo a gris
                button.setTitleColor(.darkGray, for: .normal) // Cambiar el color del texto a gris oscuro
            }
            
            view.addSubview(button)
        }
    }

    @objc func paymentMethodSelected(_ sender: UIButton) {
        let paymentMethod = sender.title(for: .normal)
        
        switch paymentMethod {
        case "Tarjeta de Crédito", "Tarjeta de Débito":
            showCardDetailsModal(for: paymentMethod!)
        case "Pagar Escaneando QR":
            showQRModal()
        case "WebPay":
            showWebPayModal()
        default:
            break
        }
    }

    func showCardDetailsModal(for method: String) {
            let alertController = UIAlertController(title: method, message: "Ingrese los datos de su tarjeta", preferredStyle: .alert)
            
            alertController.addTextField { textField in
                textField.placeholder = "Nombre completo"
                textField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
            }
            alertController.addTextField { textField in
                textField.placeholder = "RUT"
                textField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
            }
            alertController.addTextField { textField in
                textField.placeholder = "Número de tarjeta"
                textField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
            }
            alertController.addTextField { textField in
                textField.placeholder = "Fecha de vigencia"
                textField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
            }
            alertController.addTextField { textField in
                textField.placeholder = "Código de seguridad"
                textField.isSecureTextEntry = true
                textField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
            }
            
            let confirmAction = UIAlertAction(title: "Confirmar", style: .default) { [weak self, weak alertController] _ in
                var errorMessages = [String]()
                
                guard let alertController = alertController else { return }
                
                if alertController.textFields?[0].text?.isEmpty ?? true {
                    errorMessages.append("Nombre completo es obligatorio.")
                }
                if alertController.textFields?[1].text?.isEmpty ?? true {
                    errorMessages.append("RUT es obligatorio.")
                } else if let rut = alertController.textFields?[1].text, !self!.validarRUT(rut) {
                    errorMessages.append("RUT no es válido.")
                }
                if alertController.textFields?[2].text?.isEmpty ?? true {
                        errorMessages.append("Número de tarjeta es obligatorio.")
                    } else if let numeroTarjeta = alertController.textFields?[2].text, !(self?.isValidCardNumber(numeroTarjeta) ?? false) {
                        errorMessages.append("Número de tarjeta debe ser de 16 dígitos numéricos.")
                }
                if alertController.textFields?[3].text?.isEmpty ?? true {
                    errorMessages.append("Fecha de vigencia es obligatoria.")
                } else if let fechaVigencia = alertController.textFields?[3].text, !(self?.isValidExpiryDate(fechaVigencia) ?? false) {
                    errorMessages.append("Fecha de vigencia debe estar en el formato MM/AA.")
                }
                if alertController.textFields?[4].text?.isEmpty ?? true {
                    errorMessages.append("Código de seguridad es obligatorio.")
                } else if let codigoSeguridad = alertController.textFields?[4].text, !(self?.isValidSecurityCode(codigoSeguridad) ?? false) {
                    errorMessages.append("Código de seguridad debe ser de 3 dígitos numéricos.")
                }

                
                if !errorMessages.isEmpty {
                             self?.mostrarErrorEnAlerta(alertController: alertController, mensaje: errorMessages.joined(separator: "\n"))
                             return
                         }
                
                // Si la validación es exitosa, habilitar el botón para continuar
                self?.btnButton.isEnabled = true
                alertController.dismiss(animated: true, completion: nil)
            }
            
            // Agregar la acción pero no cerrar el alertController de inmediato
            alertController.addAction(confirmAction)
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            present(alertController, animated: true, completion: nil)
        }
    
    func isValidSecurityCode(_ code: String) -> Bool {
        let regex = "^[0-9]{3}$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: code)
    }
    func isValidCardNumber(_ number: String) -> Bool {
        let regex = "^[0-9]{16}$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: number)
    }
    func isValidExpiryDate(_ date: String) -> Bool {
        let regex = "^(0[1-9]|1[0-2])/([0-9]{2})$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: date)
    }
        @objc func textFieldDidChange(_ textField: UITextField) {
            guard let alertController = self.presentedViewController as? UIAlertController else { return }
            guard let nombreCompleto = alertController.textFields?[0].text, !nombreCompleto.isEmpty,
                  let rut = alertController.textFields?[1].text, !rut.isEmpty, validarRUT(rut),
                  let numeroTarjeta = alertController.textFields?[2].text, !numeroTarjeta.isEmpty,
                  let fechaVigencia = alertController.textFields?[3].text, !fechaVigencia.isEmpty,
                  let codigoSeguridad = alertController.textFields?[4].text, !codigoSeguridad.isEmpty else {
                btnButton.isEnabled = false
                return
            }
            btnButton.isEnabled = true
        }

    func mostrarErrorEnAlerta(alertController: UIAlertController, mensaje: String) {
            let errorAlert = UIAlertController(title: "Error", message: mensaje, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { _ in
                alertController.dismiss(animated: true, completion: nil)
            }
            errorAlert.addAction(okAction)
            present(errorAlert, animated: true, completion: nil)
        }
    
    func validarRUT(_ rut: String) -> Bool {
            // Remover puntos y guión
            let cleanRut = rut.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: "-", with: "")
            
            // Verificar que tenga al menos 8 caracteres (7 números y 1 dígito verificador)
            guard cleanRut.count >= 8 else { return false }
            
            // Obtener el dígito verificador
            let dv = cleanRut.suffix(1).uppercased()
            
            // Obtener el número del RUT
            let numberPart = cleanRut.prefix(cleanRut.count - 1)
            
            // Validar que la parte numérica sea realmente un número
            guard let rutNumber = Int(numberPart) else { return false }
            
            // Calcular el dígito verificador esperado
            var sum = 0
            var multiplier = 2
            
            for char in numberPart.reversed() {
                if let digit = Int(String(char)) {
                    sum += digit * multiplier
                    multiplier = (multiplier == 7) ? 2 : multiplier + 1
                }
            }
            
            let expectedDV = 11 - (sum % 11)
            let expectedDVString = (expectedDV == 11) ? "0" : (expectedDV == 10) ? "K" : String(expectedDV)
            
            return dv == expectedDVString
        }
    
    func showQRCodeModal() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let qrCodeVC = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as? QRCodeViewController {
            self.present(qrCodeVC, animated: true, completion: nil)
        }
    }

    func showWebPayModal() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let webPayVC = storyboard.instantiateViewController(withIdentifier: "WebPayViewController") as? WebPayViewController {
            self.present(webPayVC, animated: true, completion: nil)
        }
    }
    
    func showQRModal() {
        let alertController = UIAlertController(title: "Pagar Escaneando QR", message: "Escanea el siguiente código QR para pagar.", preferredStyle: .alert)

        let qrImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        qrImageView.image = generateQRCode(from: "TuTextoParaElQRCode")
        
        alertController.view.addSubview(qrImageView)
        
        qrImageView.translatesAutoresizingMaskIntoConstraints = false
        qrImageView.centerXAnchor.constraint(equalTo: alertController.view.centerXAnchor).isActive = true
        qrImageView.bottomAnchor.constraint(equalTo: alertController.view.bottomAnchor, constant: -45).isActive = true

        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        
        present(alertController, animated: true, completion: nil)
    }

    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }

    func showPaymentDetailsModal(for method: String?) {
        let alertController = UIAlertController(title: method, message: "Por favor, ingrese los detalles del pago.", preferredStyle: .alert)

        alertController.addTextField { textField in
            textField.placeholder = "Nombre Completo"
        }

        alertController.addTextField { textField in
            textField.placeholder = "RUT"
        }

        alertController.addTextField { textField in
            textField.placeholder = "Número de Tarjeta"
            textField.keyboardType = .numberPad
        }

        alertController.addTextField { textField in
            textField.placeholder = "Fecha de Vigencia (MM/AA)"
            textField.keyboardType = .numberPad
        }

        alertController.addTextField { textField in
            textField.placeholder = "Código de Seguridad"
            textField.keyboardType = .numberPad
        }

        let payAction = UIAlertAction(title: "Pagar", style: .default) { _ in
            // Manejar el pago aquí
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)

        alertController.addAction(payAction)
        alertController.addAction(cancelAction)

        present(alertController, animated: true, completion: nil)
    }

    
     

    
 
    // Función para formatear el total en formato de moneda
    func formatCurrency(_ amountString: String) -> String {
        guard let amount = Double(amountString) else {
            return "$0.00"
        }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale.current
        return formatter.string(from: NSNumber(value: amount)) ?? "$0.00"
    }
    
   
    
    @IBAction func volver() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
