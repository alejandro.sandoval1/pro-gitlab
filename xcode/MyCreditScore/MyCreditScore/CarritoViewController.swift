import UIKit

class CarritoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
  
    @IBOutlet weak var labelError: UILabel!
    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var totalLabel: UILabel!
    
    var cartItems: [Item] = []

    var errorMessage: String?
    
    override func viewDidLoad() {
       super.viewDidLoad()
       labelError.text = errorMessage
       labelError.isHidden = true
       cartTableView.delegate = self
       cartTableView.dataSource = self
       // Registrar la clase de celda estándar
       cartTableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
       
       // Recargar la tabla con los datos recuperados
        // Recargar la tabla con los datos recuperados
        if let savedItems = loadItemsFromSession() {
            cartItems = savedItems
            cartTableView.reloadData()
        } else {
            labelError.isHidden = false
        }
       
       updateTotal()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartItems.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        let item = cartItems[indexPath.row]
        
        let nameLabel = UILabel()
        nameLabel.text = item.nombre
        nameLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        // Obtener el número de elementos
        //let itemCount = cartItems.count
        // Sumar todos los valores de totalValue de los items
        //let total = cartItems.reduce(0.0) { $0 + ($1.totalValue ?? 0.0) }
        
        // Asignar el total a totalLabel.text en formato de moneda
        //totalLabel.text = "Total: \(formatCurrency(total)) - \(itemCount) elementos"
        
        let priceLabel = UILabel()
        priceLabel.text = formatCurrency(item.totalValue ?? 0)
        priceLabel.font = UIFont.systemFont(ofSize: 15)
        priceLabel.textAlignment = .right
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let stackView = UIStackView(arrangedSubviews: [nameLabel, priceLabel])
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 10
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        cell.contentView.addSubview(stackView)
        
        // Constraints
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor, constant: 16),
            stackView.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor, constant: -16),
            stackView.topAnchor.constraint(equalTo: cell.contentView.topAnchor, constant: 8),
            stackView.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor, constant: -8)
        ])
        
        // Cell appearance
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        // Actualizar el totalLabel
        updateTotalLabel()
       
        return cell
    }

    func updateTotalLabel() {
        // Sumar todos los valores de totalValue de los items
        let total = cartItems.reduce(0.0) { $0 + ($1.totalValue ?? 0.0) }
        let itemCount = cartItems.count
        
        let totalText = "Total: \(formatCurrency(total))"
        let itemCountText = " - \(itemCount) elementos"
        
        // Crear un NSMutableAttributedString con el texto total
        let attributedString = NSMutableAttributedString(string: totalText, attributes: [
            .font: UIFont.systemFont(ofSize: 17, weight: .bold),
            .foregroundColor: UIColor.black
        ])
        
        // Crear otro NSMutableAttributedString para el número de elementos
        let itemCountAttributedString = NSMutableAttributedString(string: itemCountText, attributes: [
            .font: UIFont.systemFont(ofSize: 15),
            .foregroundColor: UIColor.gray
        ])
        
        // Añadir el número de elementos al texto total
        //attributedString.append(itemCountAttributedString)
        
        // Asignar el texto estilizado al totalLabel
        totalLabel.attributedText = attributedString
    }



    func obtenerMontoTotal() -> String {
        // Extraer el monto total del texto en totalLabel
        if let totalText = totalLabel.text?.replacingOccurrences(of: "Total: ", with: "") {
            return totalText
        }
        return "0.0"
    }

       
    // Función para formatear el total en formato de moneda
    func formatCurrency(_ amount: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale.current
        return formatter.string(from: NSNumber(value: amount)) ?? "$0.00"
    }

    func updateTotal() {
        let total = cartItems.reduce(0) { $0 + ($1.price ?? 0) }
        totalLabel.text = "Total: \(formatCurrency(total))"
    }
     
    func loadItemsFromSession() -> [Item]? {
        if let savedData = UserDefaults.standard.data(forKey: "savedItems") {
            let decoder = JSONDecoder()
            do {
                let savedItems = try decoder.decode([Item].self, from: savedData)
                for item in savedItems {
                    print("Nombre: \(item.nombre), Precio: \(item.price ?? 0)")
                }
                return savedItems
            } catch {
                print("Error decoding items: \(error.localizedDescription)")
            }
        }
        return nil
    }
    
    @IBAction func pagar(_ sender: Any) {
       // Realizar la transición a PagarViewController
       performSegue(withIdentifier: "MediosViewController", sender: self)
    }
       
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if segue.identifier == "MediosViewController" {
           if let pagarVC = segue.destination as? MediosViewController {
               // Pasar el monto total a MediosViewController
               pagarVC.totalAmount = obtenerMontoTotal()
           }
       }
    }
    func mostrarError(mensaje: String) {
        DispatchQueue.main.async {
            self.labelError.isHidden = false
            self.labelError.textColor = .red
            self.labelError.text = mensaje
        }
    }
    

    @IBAction func volver(_ sender: Any) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func backResumen() {
        self.dismiss(animated: true, completion: nil)
    }
}
