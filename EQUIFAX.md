# Guía Paso a Paso para Generar un Token de Acceso

Para generar un token de acceso para pruebas en un entorno de sandbox utilizando la API de Equifax, típicamente seguirás estos pasos. Estas instrucciones asumen que ya has obtenido tu `Client ID` y `Client Secret` desde el portal de desarrolladores de Equifax.

## Iniciar Sesión en el Portal del Desarrollador

- Comienza por iniciar sesión en el portal de desarrolladores de Equifax donde registraste tu aplicación.

## Localizar la Documentación de Referencia de la API

- Busca la sección de Referencia de la API en el portal. Esta sección proporcionará documentación detallada sobre cómo interactuar con la API, incluyendo la autenticación.

## Encontrar el Punto de Autenticación

- Dentro de la documentación de la API, localiza el punto de autenticación. Aquí es donde enviarás una solicitud para obtener tu token de acceso.

## Preparar la Solicitud de Autenticación

- Necesitarás hacer una solicitud POST al punto de autenticación. Típicamente, esta solicitud incluye encabezados y cuerpo como sigue:

  - **Endpoint**: Usualmente algo como `https://api.sandbox.equifax.com/v1/oauth/token`
  - **Headers**: Incluir `Content-Type: application/x-www-form-urlencoded`
  - **Body**: Esto incluirá tu `client_id`, `client_secret`, `grant_type` y `scope`. El `grant_type` es usualmente `client_credentials`.

## Establecer el Ámbito

- El parámetro `scope` define a qué partes de la API estás solicitando acceso. Debes especificar el ámbito según el producto al que estás suscrito. Cada producto podría tener un ámbito diferente, que debe estar listado en la documentación de la API o en los detalles de tu suscripción al producto.

## Enviar la Solicitud

- Utiliza una herramienta como `curl`, Postman o cualquier cliente HTTP de tu elección para enviar la solicitud. Aquí tienes un ejemplo usando `curl`:

  ```bash
  curl --location --request POST 'https://api.sandbox.equifax.com/v1/oauth/token' \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data-urlencode 'client_id=TU_CLIENT_ID' \
  --data-urlencode 'client_secret=TU_CLIENT_SECRET' \
  --data-urlencode 'grant_type=client_credentials' \
  --data-urlencode 'scope=TU_AMBITO_PRODUCTO'

Reemplaza TU_CLIENT_ID, TU_CLIENT_SECRET, y TU_AMBITO_PRODUCTO con tus credenciales y ámbito reales.

## Recibir y Almacenar Tu Token de Acceso
La respuesta de la API incluirá tu token de acceso si la solicitud es exitosa. Este token es típicamente un JWT (JSON Web Token) y se utiliza para autenticar llamadas de API subsiguientes.

## Usar el Token de Acceso

Incluye este token de acceso en el encabezado HTTP de tus solicitudes de API subsiguientes, usualmente como Authorization: Bearer TU_TOKEN_DE_ACCESO.

![Enroller](./images/enroller.png "Enroller Key")
