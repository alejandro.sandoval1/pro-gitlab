const AWS = require('aws-sdk');
const { v4: uuidv4 } = require('uuid');

AWS.config.update({ region: 'us-east-1' });
const s3 = new AWS.S3();
const rekognition = new AWS.Rekognition();
const dynamoDB = new AWS.DynamoDB.DocumentClient();

// Define el nombre de tu bucket y la tabla de DynamoDB
const BUCKET_NAME = 'kyc-mycreditscore';
const TABLE_NAME = 'KYC'; // Nombre de la tabla de DynamoDB

exports.apikyc1 = async (event) => {
    try {
        const body = JSON.parse(event.body);

        // Validación de que todos los campos requeridos estén presentes
        const requiredFields = ['rut', 'b64_image', 'stage'];
        for (let field of requiredFields) {
            if (!body[field]) {
                return {
                    statusCode: 400,
                    body: JSON.stringify({ message: `El campo ${field} es requerido.` }),
                };
            }
        }

        const { rut, b64_image, stage } = body;
        const id = uuidv4(); // Genera un ID único para el registro

        // Subir la imagen de la cédula (stage 1) a S3 antes de procesar
        if (stage === 1) {
            const s3ImageUrl = await uploadIDCardToS3(b64_image, rut);
            if (!s3ImageUrl) {
                return {
                    statusCode: 500,
                    body: JSON.stringify({ message: 'Error al subir la imagen al bucket S3' }),
                };
            }

            // Guardar el registro en DynamoDB con el stage 1
            await saveToDynamoDB({ id, rut, stage, imageUrl: s3ImageUrl });

            return {
                statusCode: 200,
                body: JSON.stringify({ message: 'Imagen del documento subida y registrada exitosamente', s3ImageUrl, stage }),
            };
        }

        // Guardar los datos en DynamoDB para cada stage
        await saveToDynamoDB({ id, rut, stage, b64_image });

        // Si es el tercer stage (selfie), extraer el rostro y comparar
        if (stage === 3) {
            const selfieImage = b64_image;
            const extractedFaceUrl = await extractAndCompareFaceFromIDCard(rut, selfieImage);

            if (!extractedFaceUrl) {
                return {
                    statusCode: 400,
                    body: JSON.stringify({ message: 'Error al procesar la imagen o comparar el rostro con la selfie' }),
                };
            }

            return {
                statusCode: 200,
                body: JSON.stringify({ message: 'Validación KYC exitosa', id: id, imageUrl: extractedFaceUrl, step: 3 }),
            };
        }

        return {
            statusCode: 200,
            body: JSON.stringify({ message: 'Proceso completado exitosamente', stage }),
        };
    } catch (error) {
        console.error('Error:', error);
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Error interno del servidor' }),
        };
    }
};

// Función para subir la imagen de la cédula a S3
async function uploadIDCardToS3(b64_image, rut) {
    try {
        const idCardImageBuffer = Buffer.from(b64_image.replace(/^data:image\/\w+;base64,/, ''), 'base64');
        const imageKey = `idcard-${rut}-${uuidv4()}.jpg`;

        const uploadParams = {
            Bucket: BUCKET_NAME,
            Key: imageKey,
            Body: idCardImageBuffer,
            ContentType: 'image/jpeg',
        };

        const uploadResult = await s3.upload(uploadParams).promise();
        console.log(`Imagen del documento subida a S3: ${uploadResult.Location}`);

        return uploadResult.Location; // Retorna la URL completa de la imagen en S3
    } catch (error) {
        console.error('Error al subir la imagen a S3:', error);
        return null;
    }
}

// Función para guardar datos en DynamoDB
async function saveToDynamoDB(record) {
    const params = {
        TableName: TABLE_NAME,
        Item: record,
    };

    try {
        await dynamoDB.put(params).promise();
        console.log('Registro guardado en DynamoDB:', record);
    } catch (error) {
        console.error('Error al guardar en DynamoDB:', error);
    }
}

// Función para extraer el rostro de la cédula almacenada en S3 y compararlo con la selfie
async function extractAndCompareFaceFromIDCard(rut, selfieImage) {
    try {
        // Obtener la URL de la imagen del stage 1 desde DynamoDB
        const s3ImageUrl = await getS3ImageKeyForRut(rut, 1);  // Obtener la URL de la imagen del stage 1

        if (!s3ImageUrl) {
            console.error('No se encontró la imagen de la cédula en la base de datos');
            return null;
        }
        console.log(s3ImageUrl);

        // Descargar la imagen desde S3
        const s3Key = s3ImageUrl.split('.com/')[1]; // Extraer el key de la URL completa
        const imageBuffer = await downloadImageFromS3(s3Key);

        // Aquí podrías agregar la lógica de recorte si es necesario, usando librerías alternativas

        // Ahora, comparar la imagen recortada con la selfie usando Rekognition
        const isFaceMatch = await compareImagesWithRekognition(imageBuffer, selfieImage);

        if (isFaceMatch) {
            return s3ImageUrl; // Retornar la URL pública de la imagen si coinciden
        } else {
            console.error('Las imágenes no coinciden');
            return null;
        }
    } catch (error) {
        console.error('Error al procesar la imagen:', error);
        return null;
    }
}

// Función para descargar la imagen de S3 sin Jimp
async function downloadImageFromS3(s3ImageKey) {
    try {
        const s3Params = {
            Bucket: BUCKET_NAME,
            Key: s3ImageKey
        };
        const s3Data = await s3.getObject(s3Params).promise();

        // Devolver la imagen en buffer
        return s3Data.Body;
    } catch (error) {
        console.error('Error al descargar la imagen desde S3:', error);
        throw error;
    }
}

// Función para usar Amazon Rekognition y comparar las imágenes
async function compareImagesWithRekognition(croppedImageBuffer, selfieImageBase64) {
    const params = {
        SourceImage: {
            Bytes: croppedImageBuffer,  // Imagen recortada en buffer
        },
        TargetImage: {
            Bytes: Buffer.from(selfieImageBase64.replace(/^data:image\/\w+;base64,/, ''), 'base64'),  // Imagen selfie en Base64
        },
        SimilarityThreshold: process.env.UMBRAL,  // Puedes ajustar el umbral de similitud según lo requerido
    };

    try {
        const result = await rekognition.compareFaces(params).promise();
        const similarity = result.FaceMatches.length > 0 ? result.FaceMatches[0].Similarity : 0;

        // Retorna true si la similitud es igual o mayor al umbral especificado
        return similarity >= process.env.UMBRAL;
    } catch (error) {
        console.error('Error en la comparación de imágenes:', error);
        return false;
    }
}

// Función para obtener el Key de la imagen del documento en S3 usando el rut y stage
async function getS3ImageKeyForRut(rut, stage) {
    const params = {
        TableName: TABLE_NAME,
        IndexName: 'rut-stage-index',
        KeyConditionExpression: 'rut = :rut AND stage = :stage',
        ExpressionAttributeValues: {
            ':rut': rut,
            ':stage': stage
        }
    };

    try {
        const result = await dynamoDB.query(params).promise();
        if (result.Items.length > 0) {
            return result.Items[0].imageUrl; // Retornar el campo imageUrl de DynamoDB si existe
        } else {
            console.error('No se encontró ninguna imagen para el rut y stage especificados.');
            return null;
        }
    } catch (error) {
        console.error('Error al consultar DynamoDB:', error);
        return null;
    }
}
