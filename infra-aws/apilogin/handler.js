const AWS = require('aws-sdk');
const jwt = require('jsonwebtoken');

AWS.config.update({ region: 'us-east-1' });
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.apilogin = async (event) => {
    let response;
    try {
        // 1. Parsear el cuerpo de la solicitud
        let username="", password="";

        try {
            ({ username, password } = JSON.parse(event.body));
        } catch (error) {
            console.error('Error parsing JSON:', error);
            return {
                statusCode: 400,
                headers: { "Content-Type": "application/json", "Access-Control-Allow-Origin": "*" },
                body: JSON.stringify({ message: "Invalid JSON in request body" })
            };
        }

        // 2. Buscar usuario por el nombre de usuario (username)
        const getUserParams = {
            TableName: 'Usuario',
            IndexName: 'username-index',
            KeyConditionExpression: 'username = :username',
            ExpressionAttributeValues: { ':username': username }
        };

        console.log('getUserParams:'+ JSON.stringify(getUserParams));

        const userData = await dynamoDB.query(getUserParams).promise();

        if (userData.Items.length === 0) {
            return {
                statusCode: 404,
                headers: { "Content-Type": "application/json", "Access-Control-Allow-Origin": "*" },
                body: JSON.stringify({ message: "Usuario no encontrado" })
            };
        }
        const usuario = userData.Items[0];
        
        // 3. Comparar la contraseña en texto claro
        if (password !== usuario.password) {  // usuario.password es la contraseña almacenada en la BD
            return {
                statusCode: 401,
                headers: { "Content-Type": "application/json", "Access-Control-Allow-Origin": "*" },
                body: JSON.stringify({ message: "Contraseña incorrecta" })
            };
        }

        // 3.1 carga de rut de cliente
        const clientid = usuario.cliente_id;
        const getClientParams = {
            TableName: 'Cliente',
            IndexName: 'id-index',
            KeyConditionExpression: 'id = :id',
            ExpressionAttributeValues: { ':id': clientid }
        };

        console.log('getClientParams:'+ JSON.stringify(getClientParams));

        const clientData = await dynamoDB.query(getClientParams).promise();
        if (clientData.Items.length === 0) {
            return {
                statusCode: 404,
                headers: { "Content-Type": "application/json", "Access-Control-Allow-Origin": "*" },
                body: JSON.stringify({ message: "Cliente no encontrado" })
            };
        }

        // 4. Obtener perfil del usuario
        const getPerfilParams = {
            TableName: 'Perfil',
            KeyConditionExpression: 'usuario_id = :usuario_id',
            ExpressionAttributeValues: { ':usuario_id': usuario.id }
        };

        const perfilData = await dynamoDB.query(getPerfilParams).promise();

        if (perfilData.Items.length === 0) {
            return {
                statusCode: 404,
                headers: { "Content-Type": "application/json", "Access-Control-Allow-Origin": "*" },
                body: JSON.stringify({ message: "Perfil no encontrado" })
            };
        }

        const rolId = perfilData.Items[0].rol_id;

        // 5. Obtener rol
        const getRolParams = {
            TableName: 'Rol',
            Key: { id: rolId }
        };

        const rolData = await dynamoDB.get(getRolParams).promise();

        if (!rolData.Item) {
            return {
                statusCode: 404,
                headers: { "Content-Type": "application/json", "Access-Control-Allow-Origin": "*" },
                body: JSON.stringify({ message: "Rol no encontrado" })
            };
        }

        // 6. Generar el token JWT
        const tokenPayload = {
            usuarioId: usuario.id,
            username: username,
            email: userData.Email,
            rolId: rolId
        };

        const token = jwt.sign(tokenPayload, 'clave-super-secreta', { expiresIn: '1h' });

        // 7. Respuesta exitosa
        response = {
            statusCode: 200,
            headers: { "Content-Type": "application/json", "Access-Control-Allow-Origin": "*" },
            body: JSON.stringify({
                message: "¡ Acceso OK !",
                cliente: clientData.Items[0],
                usuario: usuario,
                perfil: perfilData.Items[0],
                rol: rolData.Item,
                token: token
            })
        };
    } catch (error) {
        console.error("Error:", error);
        response = {
            statusCode: 500,
            headers: { "Content-Type": "application/json", "Access-Control-Allow-Origin": "*" },
            body: JSON.stringify({
                message: "Error interno del servidor",
                error: error.message
            })
        };
    }
    return response;
};