#!/bin/bash

# URL de inicio de sesión
LOGIN_URL="https://f8o07uskqc.execute-api.us-east-1.amazonaws.com/Prod/mycreditscore/api/login"

# URL de items
ITEMS_URL="https://gyyyzxyquf.execute-api.us-east-1.amazonaws.com/Prod/mycreditscore/api/items"

# Datos de inicio de sesión
LOGIN_DATA='{"username": "Monica", "password": "Araneda"}'

# Obtener el token
TOKEN=$(curl -s -X POST -H "Content-Type: application/json" -d "$LOGIN_DATA" "$LOGIN_URL" | jq -r '.token')

echo "$TOKEN"

# Verificar si el token fue obtenido correctamente
# if [ -z "$TOKEN" ]; then
#     echo "Error: No se pudo obtener el token de autenticación"
#     exit 1
# fi

# # Mostrar el token para depuración (quitar después de verificar)
# echo "Token obtenido: $TOKEN"

# # Realizar la solicitud GET usando AWS CLI con AWS Signature Version 4
# RESPONSE=$(aws --region us-east-1 --output json \
#   --cli-binary-format raw-in-base64-out \
#   apigatewayv2 get-authorizers \
#   --api-id gyyyzxyquf \
#   --query 'Items[?name==`mycreditscore-authorizer`].id')

# # Verificar si la solicitud fue exitosa
# HTTP_STATUS=$(echo "$RESPONSE" | jq -r '.statusCode // empty')

# if [ -z "$HTTP_STATUS" ] || [ "$HTTP_STATUS" != "200" ]; then
#     echo "Error: La solicitud a la API de items falló con el estado HTTP $HTTP_STATUS"
#     echo "Respuesta completa: $RESPONSE"
#     exit 1
# fi

# Imprimir la respuesta de la API de items
# echo "$RESPONSE"
