const jwt = require('jsonwebtoken');

exports.authorizerAPI = async (event) => {
    const token = event.authorizationToken.replace("Bearer ", "");
    try {
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const policyDocument = generatePolicy(decoded.sub, 'Allow', event.methodArn);
        return {
            principalId: decoded.sub,
            policyDocument,
            context: {
                user: JSON.stringify(decoded)
            }
        };
    } catch (err) {
        console.error('Token verification failed:', err);
        return {
            principalId: 'user',
            policyDocument: generatePolicy('user', 'Deny', event.methodArn),
            context: {
                error: err.message
            }
        };
    }
};

const generatePolicy = (principalId, effect, resource) => {
    const policyDocument = {
        Version: '2012-10-17',
        Statement: [
            {
                Action: 'execute-api:Invoke',
                Effect: effect,
                Resource: resource
            }
        ]
    };
    return policyDocument;
};
