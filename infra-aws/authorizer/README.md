# Autorizador Personalizado de AWS Lambda

Este repositorio contiene una función de AWS Lambda en Node.js diseñada para actuar como un autorizador personalizado para AWS API Gateway. Esta función evalúa los tokens de autorización entrantes y otorga o deniega el acceso basado en el valor del token.

## Visión General

La función de autorizador personalizado `exports.authorizer` verifica el token de autorización proporcionado en las solicitudes de API. Si el token es igual a "allow", la función otorga acceso a la API; de lo contrario, lo deniega.

## Componentes Clave

- `exports.authorizer`: La función principal que AWS Lambda invoca. Procesa eventos entrantes con un token de autorización y un ARN de método (Amazon Resource Name).
- `generateAuthResponse`: Función auxiliar que construye un documento de política IAM basado en la decisión de autorización.
- `generatePolicyDocument`: Función auxiliar que crea un documento de política IAM que especifica los permisos de invocación de la API.

## Funcionamiento

La función Lambda evalúa si el `authorizationToken` entrante es "allow". Si es así, devuelve una política IAM con un efecto "Allow" para el ARN del método solicitado. Si el token es cualquier otra cosa, devuelve una política con efecto "Deny".

## Configuración

### Requisitos

- Node.js (versión recomendada 12.x o posterior)
- AWS CLI configurado en tu máquina
- Cuenta de AWS y permisos adecuados para crear funciones Lambda y configuraciones de API Gateway

### Despliegue

1. **Crear una Función Lambda:**
   - Navega al AWS Management Console.
   - Ve a AWS Lambda y crea una nueva función Lambda.
   - Elige el runtime como Node.js.
   - Pega el código proporcionado en el editor en línea en la consola de AWS Lambda.

2. **Configurar el API Gateway:**
   - Crea una nueva API o utiliza una existente en AWS API Gateway.
   - Configura un autorizador personalizado para los métodos de tu API:
      - Utiliza la función Lambda que creaste como el autorizador.
      - Configura el disparador de Lambda en las configuraciones del API Gateway.

3. **Desplegar la API:**
   - Despliega tu API en AWS API Gateway.
   - Prueba los endpoints con y sin el token "allow".

## Pruebas

Puedes probar el autorizador haciendo solicitudes HTTP a tu endpoint de API:

- Utiliza una herramienta como Postman o cURL.
- Incluye un encabezado `Authorization` con valores "allow" o cualquier otra cadena para probar las respuestas.

Ejemplo de comando cURL:

```bash
curl -X GET https://tu-id-api.execute-api.tu-region.amazonaws.com/escenario/recurso \
-H 'Authorization: allow'
