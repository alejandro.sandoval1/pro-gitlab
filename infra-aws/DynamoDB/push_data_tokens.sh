#!/bin/bash

generate_token_value() {
  python3 -c "import random; import string; print(''.join(random.choices(string.ascii_uppercase + string.digits, k=6)))"
}

for i in {1..5}
do
  id=$(uuidgen)
  userId=$(uuidgen)
  tokenId=$(uuidgen)
  tokenValue=$(generate_token_value)
  createdAt=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
  expiresAt=$(date -u -v +45S +"%Y-%m-%dT%H:%M:%SZ")

  aws dynamodb put-item \
    --table-name Tokens \
    --item "{
      \"id\": {\"S\": \"$id\"},
      \"userId\": {\"S\": \"$userId\"},
      \"tokenId\": {\"S\": \"$tokenId\"},
      \"tokenValue\": {\"S\": \"$tokenValue\"},
      \"createdAt\": {\"S\": \"$createdAt\"},
      \"expiresAt\": {\"S\": \"$expiresAt\"}
    }"
done
