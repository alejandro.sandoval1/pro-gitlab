const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event, context) => {
    const params = {
        TableName: process.env.TABLE_NAME,
        Item: {
            tenantId: '12345678',
            empresa: 'Eserp Agency'
            }
        };
    
        try {
            await dynamoDb.put(params).promise();
            return { statusCode: 200, body: 'Data seeded successfully!' };
        } catch (err) {
            console.error('Error seeding data:', err);
            return { statusCode: 500, body: 'Failed to seed data' };
        }
};
