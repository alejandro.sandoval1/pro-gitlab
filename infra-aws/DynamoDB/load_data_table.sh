aws dynamodb put-item --table-name Usuario --item '{
    "id": {"S": "uuid0"},
    "username": {"S": "Monica"},
    "email": {"S": "monica@gmail.com"},
    "password": {"S": "Araneda"},
    "fecha_creacion": {"S": "2023-05-09"},
    "fecha_actualizacion": {"S": "2023-05-09"}
}'

aws dynamodb put-item --table-name Usuario --item '{
    "id": {"S": "uuid1"},
    "username": {"S": "user1"},
    "email": {"S": "user1@example.com"},
    "password": {"S": "password1"},
    "fecha_creacion": {"S": "2023-01-01"},
    "fecha_actualizacion": {"S": "2023-01-02"}
}'

aws dynamodb put-item --table-name Usuario --item '{
    "id": {"S": "uuid2"},
    "username": {"S": "user2"},
    "email": {"S": "user2@example.com"},
    "password": {"S": "password2"},
    "fecha_creacion": {"S": "2023-02-01"},
    "fecha_actualizacion": {"S": "2023-02-02"}
}'

aws dynamodb put-item --table-name Usuario --item '{
    "id": {"S": "uuid3"},
    "username": {"S": "user3"},
    "email": {"S": "user3@example.com"},
    "password": {"S": "password3"},
    "fecha_creacion": {"S": "2023-03-01"},
    "fecha_actualizacion": {"S": "2023-03-02"}
}'


# Rol 
aws dynamodb put-item --table-name Rol --item '{
    "id": {"S": "uuid4"},
    "nombre": {"S": "admin"},
    "descripcion": {"S": "Administrador del sistema"}
}'

aws dynamodb put-item --table-name Rol --item '{
    "id": {"S": "uuid5"},
    "nombre": {"S": "editor"},
    "descripcion": {"S": "Editor de contenido"}
}'

aws dynamodb put-item --table-name Rol --item '{
    "id": {"S": "uuid6"},
    "nombre": {"S": "viewer"},
    "descripcion": {"S": "Visualizador de contenido"}
}'

#Perfil

aws dynamodb put-item --table-name Perfil --item '{
    "usuario_id": {"S": "uuid0"},
    "rol_id": {"S": "uuid6"},
    "descripcion": {"S": "Perfil del usuario 0 con rol Visualizador"}
}'

aws dynamodb put-item --table-name Perfil --item '{
    "usuario_id": {"S": "uuid1"},
    "rol_id": {"S": "uuid4"},
    "descripcion": {"S": "Perfil del usuario 1 con rol admin"}
}'

aws dynamodb put-item --table-name Perfil --item '{
    "usuario_id": {"S": "uuid2"},
    "rol_id": {"S": "uuid5"},
    "descripcion": {"S": "Perfil del usuario 2 con rol editor"}
}'

aws dynamodb put-item --table-name Perfil --item '{
    "usuario_id": {"S": "uuid3"},
    "rol_id": {"S": "uuid6"},
    "descripcion": {"S": "Perfil del usuario 3 con rol viewer"}
}'
