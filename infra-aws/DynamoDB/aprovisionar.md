 

## Aprovisionamiento
 
```bash  
# Registro para la tabla Cliente
aws dynamodb put-item \
    --table-name Cliente \
    --item '{
        "id": {"S": "123e4567-e89b-12d3-a456-426614174000"},
        "rut": {"S": "12345678-9"},
        "nombre": {"S": "Juan"},
        "apellido": {"S": "Perez"},
        "email": {"S": "juan.perez@example.com"},
        "fono": {"S": "+56912345678"},
        "fecha_creacion": {"S": "2023-01-01"},
        "fecha_actualizacion": {"S": "2024-01-01"},
        "nrodocumento": {"S": "87654321-0"}
    }'

# Registro para la tabla Tenant
aws dynamodb put-item \
    --table-name Tenant \
    --item '{
        "id": {"S": "223e4567-e89b-12d3-a456-426614174000"},
        "rut": {"S": "13.668.945-2"},
        "nombre": {"S": "Eserp Agency"},
        "apellido": {"S": "Agency"},
        "email": {"S": "maravena@eserp.cl"},
        "fono": {"S": "+56987654321"},
        "fecha_creacion": {"S": "2023-01-01"},
        "fecha_actualizacion": {"S": "2024-01-01"}
    }'


# Registro para la tabla Servicio
aws dynamodb put-item \
    --table-name Servicio \
    --item '{
        "id": {"S": "323e4567-e89b-12d3-a456-426614174000"},
        "nombre": {"S": "Servicio 01"},
        "descripcion": {"S": "Descripcion del Servicio 01"},
        "fecha_creacion": {"S": "2023-01-01"},
        "fecha_actualizacion": {"S": "2024-01-01"},
        "condiciones": {"S": "Condiciones del Servicio 01"}
    }'

# Registro para la tabla Producto
aws dynamodb put-item \
    --table-name Producto \
    --item '{
        "id": {"S": "423e4567-e89b-12d3-a456-426614174000"},
        "nombre": {"S": "Producto 01"},
        "descripcion": {"S": "Descripcion del Producto 01"},
        "categoria": {"S": "Categoria del Producto 01"},
        "fecha_creacion": {"S": "2023-01-01"},
        "fecha_actualizacion": {"S": "2024-01-01"}
    }'

# Registro para la tabla Producto_Servicio
aws dynamodb put-item \
    --table-name Producto_Servicio \
    --item '{
        "id": {"S": "523e4567-e89b-12d3-a456-426614174000"},
        "producto_id": {"S": "423e4567-e89b-12d3-a456-426614174000"},
        "servicio_id": {"S": "323e4567-e89b-12d3-a456-426614174000"},
        "fecha": {"S": "2023-01-01"},
        "descripcion": {"S": "Descripcion de la relacion Producto-Servicio"}
    }'

# Registro para la tabla Trx Movimientos
aws dynamodb put-item \
    --table-name Trx_Movimientos \
    --item '{
        "id": {"S": "623e4567-e89b-12d3-a456-426614174000"},
        "tenant_id": {"S": "223e4567-e89b-12d3-a456-426614174000"},
        "producto_id": {"S": "423e4567-e89b-12d3-a456-426614174000"},
        "servicio_id": {"S": "323e4567-e89b-12d3-a456-426614174000"},
        "fecha": {"S": "2023-01-01"},
        "estado": {"S": "activo"},
        "entidad": {"S": "PERSONA"}
    }'

# Registro para la tabla Operacion
aws dynamodb put-item \
    --table-name Operacion \
    --item '{
        "id": {"S": "723e4567-e89b-12d3-a456-426614174000"},
        "id_movimiento": {"S": "623e4567-e89b-12d3-a456-426614174000"},
        "descripcion": {"S": "Descripcion de la Operacion"},
        "estado": {"S": "pendiente"},
        "token_autorizacion": {"S": "token123"}
    }'

# Registro para la tabla Tarifas
aws dynamodb put-item \
    --table-name Tarifas \
    --item '{
        "id": {"S": "823e4567-e89b-12d3-a456-426614174000"},
        "servicio_id": {"S": "323e4567-e89b-12d3-a456-426614174000"},
        "descripcion": {"S": "Descripcion de la Tarifa"},
        "monto": {"N": "100.0"},
        "moneda": {"S": "USD"},
        "periodo": {"S": "Mensual"},
        "fecha_inicio": {"S": "2023-01-01"},
        "fecha_fin": {"S": "2023-12-31"}
    }'

# Registro para la tabla Suscripcion
aws dynamodb put-item \
    --table-name Suscripcion \
    --item '{
        "id": {"S": "923e4567-e89b-12d3-a456-426614174000"},
        "tenant_id": {"S": "223e4567-e89b-12d3-a456-426614174000"},
        "producto_id": {"S": "423e4567-e89b-12d3-a456-426614174000"},
        "estado": {"S": "activa"},
        "fecha_creacion": {"S": "2023-01-01"},
        "fecha_modificacion": {"S": "2024-01-01"}
    }'

# Registro de Solicitudes 
aws dynamodb put-item \
    --table-name Tokens \
    --item '{
        "userId": {"S": "user123"},
        "tokenId": {"S": "token456"},
        "tokenValue": {"S": "xyz789"},
        "createdAt": {"S": "2024-07-21T12:34:56Z"},
        "expiresAt": {"S": "2024-07-21T12:35:41Z"}
    }'



```