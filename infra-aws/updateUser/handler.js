const AWS = require('aws-sdk');
const nodemailer = require('nodemailer');
const fs = require('fs');
const path = require('path');

AWS.config.update({region: 'us-east-1'});
const dynamoDB = new AWS.DynamoDB.DocumentClient();
const ses = new AWS.SES({ region: process.env.REGION });

exports.updateUser = async (event) => {
    // Validar entrada
    if (!event.body) {
        return {
            statusCode: 400,
            body: JSON.stringify({ message: 'Entrada inválida' })
        };
    }

    const { email, password } = JSON.parse(event.body);

    if (!email || !password) {
        return {
            statusCode: 400,
            body: JSON.stringify({ message: 'Faltan parámetros' })
        };
    }

    // Consulta para obtener el userId usando el índice email-index
    const queryParams = {
        TableName: 'Usuario',
        IndexName: 'email-index', // Usando el índice email-index
        KeyConditionExpression: 'email = :email',
        ExpressionAttributeValues: {
            ':email': email
        }
    };

    try {
        const queryResult = await dynamoDB.query(queryParams).promise();
        
        if (queryResult.Items.length === 0) {
            return {
                statusCode: 404,
                body: JSON.stringify({ message: 'Usuario no encontrado' })
            };
        }

        const userId = queryResult.Items[0].id;

        // Actualizar la contraseña en la tabla Usuario
        const updateParams = {
            TableName: 'Usuario',
            Key: { id: userId }, // Utilizar el userId encontrado en la consulta
            UpdateExpression: 'SET password = :password',
            ExpressionAttributeValues: {
                ':password': password
            }
        };

        await dynamoDB.update(updateParams).promise();

        const userParams2 = {
            TableName: 'Cliente',
            IndexName: 'email-index',
            KeyConditionExpression: 'email = :email',
            ExpressionAttributeValues: {
                ':email': email
            }
        };
    

        const userResult2 = await dynamoDB.query(userParams2).promise();
        if (!userResult2.Items.length) {
            return {
                statusCode: 404,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify({ message: 'Cliente no existe' })
            };
        } else {
            clienteName = userResult2.Items[0].nombre + " " + userResult2.Items[0].apellido;
        }


        // Enviar correo de notificación
        const transporter = nodemailer.createTransport({
            SES: ses
        });

        const templatePath = path.join(__dirname, 'template.html');
        let htmlContent = fs.readFileSync(templatePath, 'utf8');
        htmlContent = htmlContent.replace('${cliente}', clienteName);
        htmlContent = htmlContent.replace('${message}', 'Tu contraseña ha sido actualizada exitósamente.');

        const mailOptions = {
            from: process.env.SENDER_EMAIL, // Cambia esto a tu correo verificado en SES
            to: email,
            subject: 'Notificación de Actualización de Contraseña',
            text: 'Tu contraseña ha sido actualizada exitósamente.',
            html: htmlContent,
            attachments: [
                {
                    filename: 'logo.png',
                    path: path.join(__dirname, 'assets/Image@1x.png'), // Ruta correcta del logo
                    cid: 'logo' // mismo CID que se usa en el HTML
                }
            ]
        };

        const info = await transporter.sendMail(mailOptions);

        return {
            statusCode: 200,
            body: JSON.stringify({ message: 'Usuario actualizado y correo enviado exitosamente!', info: info })
        };

    } catch (error) {
        console.error('Error al actualizar el usuario o enviar el correo:', error);
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Error interno del servidor', error: error.message })
        };
    }
};
