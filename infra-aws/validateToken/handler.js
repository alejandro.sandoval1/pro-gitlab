const { DynamoDB } = require('aws-sdk');
const AWS = require('aws-sdk');

AWS.config.update({region: 'us-east-1'});
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.validateToken = async (event) => {
    const { userId, tokenValue } = JSON.parse(event.body);

    console.log("userId:", userId);
    console.log("tokenValue:", tokenValue);
    console.log("new Date().toISOString():", new Date().toISOString());

    // Consulta para verificar si el token es válido
    const params = {
        TableName: 'Tokens',
        IndexName: 'userId-tokenValue-index',  // Asegúrate de que este sea el nombre correcto del índice
        KeyConditionExpression: 'userId = :userId AND tokenValue = :tokenValue',
        FilterExpression: 'expiresAt >= :now',
        ExpressionAttributeValues: {
            ':userId': userId,
            ':tokenValue': tokenValue,
            ':now': new Date().toISOString()
        }
    };

    

    try {
        console.log("params:", params);
        const result = await dynamoDB.query(params).promise();
        console.log("result:", result);
        if (result.Items.length === 0) {
            return {
                statusCode: 404,
                body: JSON.stringify({ message: 'Token no válido o expirado' })
            };
        }

        return {
            statusCode: 200,
            body: JSON.stringify({ message: 'Token válido' })
        };
    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Error interno del servidor' })
        };
    }
};
