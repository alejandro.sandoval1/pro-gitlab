const axios = require('axios');
const qs = require('querystring');
const AWS = require('aws-sdk');
const { v4: uuidv4 } = require('uuid');


AWS.config.update({region: 'us-east-1'}); 
const dynamoDb = new AWS.DynamoDB.DocumentClient();

exports.oauth2Token = async (event) => {
    const endpoint = 'https://api.sandbox.equifax.com/personal/consumer-data-suite/oauth/token';

    // Leer los datos enviados a través del cuerpo del evento, que es procesado por API Gateway
    const { apiKey, clientAssertion, scope = 'delivery', grantType = 'jwt-bearer' } = JSON.parse(event.body);
 

    try {

         // Generar un UUID
        insertOperation("movimiento_"+uuidv4() ,"movimiento inicial", "completado", clientAssertion);

        const response = await axios.post(endpoint, qs.stringify({
            grant_type: grantType,
            api_key: apiKey,
            client_assertion: clientAssertion,
            scope: scope
        }), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });

        return {
            statusCode: 200,
            headers: {
            "Content-Type": "application/json",
            },
            body: JSON.stringify({  
                access_token: response.data.access_token,
                token_type: response.data.token_type,
                refresh_token: response.data.refresh_token,
                expires_in: response.data.expires_in,
                scope: response.data.scope
            })
        };

    } catch (error) {
        console.error('Error al generar el oAUTH2 Token:', error);
        return {
        statusCode: error.response ? error.response.status : 500,
        body: JSON.stringify({ message: error.message })
        };
    }
};

async function insertOperation(movimiento,descripcion,estado,token) {
    const id_operacion = uuidv4();
    const fecha = new Date().toISOString();
    const params = {
        TableName: 'Operacion',
        Item: {
            id: id_operacion,
            fecha: fecha,
            estado: estado,
            id_movimiento: movimiento,
            descripcion: descripcion,
            token_autorizacion: token,
            detalles: {
                tiempo_respuesta_ms: 50,
                codigo_respuesta: 200,
                mensaje_respuesta: 'OK'
            }
        }
};

    try {
        await dynamoDb.put(params).promise();
        console.log('Transacción insertada correctamente:', params.Item);
    } catch (error) {
        console.error('Error al insertar la transacción:', error);
    }
}