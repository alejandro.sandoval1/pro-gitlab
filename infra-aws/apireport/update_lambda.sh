#!/bin/bash

# Define variables
FUNCTION_NAME="arn:aws:lambda:us-east-1:487128762439:function:stackReport-ReportFunction-z4maG0QrJohP"
PROJECT_PATH="/Users/alejandroschwartz/space/mycreditscore/pro-gitlab/infra-aws/apireport"
ZIP_FILE="lambda_function.zip"
S3_BUCKET="aws-sam-cli-managed-default-samclisourcebucket-d7hu7osu2zst"
S3_KEY="stackReport-ReportFunction-z4maG0QrJohP/zips/$ZIP_FILE"

# Navega al directorio del proyecto
cd $PROJECT_PATH || { echo "Error: No se pudo acceder al directorio del proyecto"; exit 1; }

# Instala dependencias en modo producción
npm install --production || { echo "Error: No se pudieron instalar las dependencias"; exit 1; }

# Crea el archivo ZIP
zip -r $ZIP_FILE handler.js assets node_modules package.json package-lock.json template.html || { echo "Error: No se pudo crear el archivo ZIP"; exit 1; }

# Sube el archivo ZIP a S3
aws s3 cp $ZIP_FILE s3://$S3_BUCKET/$S3_KEY || { echo "Error: No se pudo subir el archivo ZIP a S3"; exit 1; }

# Actualiza el código de la función Lambda desde S3
aws lambda update-function-code --function-name $FUNCTION_NAME --s3-bucket $S3_BUCKET --s3-key $S3_KEY || { echo "Error: No se pudo actualizar la función Lambda"; exit 1; }

# Espera a que la función esté lista para ser actualizada
echo "Esperando a que la función esté lista para actualizar la configuración..."
while true; do
    STATUS=$(aws lambda get-function --function-name $FUNCTION_NAME --query 'Configuration.LastUpdateStatus' --output text)
    if [ "$STATUS" == "Successful" ] || [ "$STATUS" == "Failed" ]; then
        break
    fi
    echo "Estado actual: $STATUS. Esperando..."
    sleep 10
done

# Actualiza la configuración de la función Lambda solo si es necesario
RUNTIME_STATUS=$(aws lambda get-function-configuration --function-name $FUNCTION_NAME --query 'Runtime' --output text)
if [ "$RUNTIME_STATUS" != "nodejs16.x" ]; then
    aws lambda update-function-configuration --function-name $FUNCTION_NAME --runtime nodejs16.x || { echo "Error: No se pudo actualizar el runtime de la función Lambda"; exit 1; }
else
    echo "El runtime ya está configurado en nodejs16.x, no es necesario actualizarlo."
fi

echo "Actualización completada."
