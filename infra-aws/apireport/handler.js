const AWS = require('aws-sdk');
const fs = require('fs');
const path = require('path');

AWS.config.update({ region: 'us-east-1' });
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.apireport = async (event) => {

    let rut;

    try {
        ({ rut } = JSON.parse(event.body));
    } catch (error) {
        console.error('Error parsing JSON:', error);
        return {
            statusCode: 400,
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify({ message: "Invalid JSON in request body" })
        };
    }

    const getReportParams = {
        TableName: 'reports',
        IndexName: 'rut-index',
        KeyConditionExpression: 'rut = :rut',
        ExpressionAttributeValues: {
            ':rut': rut
        }
    };

    let result;
    try {
        result = await dynamoDB.query(getReportParams).promise();
    } catch (error) {
        console.error('Error al consultar DynamoDB:', error);
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Error al consultar DynamoDB' }),
        };
    }

    if (result.Items.length === 0) {
        return {
            statusCode: 404,
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify({ message: "Rut no encontrado" })
        };
    }

    // Decodificar el campo JSON Base64 y parsearlo a objeto JSON
    const decodedJsonString = Buffer.from(result.Items[0].json, 'base64').toString('utf8');
    const data = JSON.parse(decodedJsonString);

    // Cargar el template HTML
    const templatePath = path.resolve(__dirname, 'template.html');
    let htmlContent = fs.readFileSync(templatePath, 'utf8');

    console.log('data:', data)
    // Reemplazar las variables en el template HTML
    let fulldate = new Date().toLocaleDateString('es-ES', { day: '2-digit', month: '2-digit', year: 'numeric' }) + ' ' + new Date().toLocaleTimeString('es-ES', { hour: '2-digit', minute: '2-digit' })

    try {

        const replacements = {
            '${nombre}': data.platinum360.getInformePlatinum360Response.response.PersonGeneral.Names.PersonName,
            '${rut}': formatearRUT(data.platinum360.getInformePlatinum360Response.response.PersonGeneral.Rut),
            '${genero}': data.platinum360.getInformePlatinum360Response.response.PersonGeneral.Gender === 'F' ? 'Femenino' : 'Masculino',
            '${edad}': data.platinum360.getInformePlatinum360Response.response.PersonGeneral.Age,
            '${nacionalidad}': data.platinum360.getInformePlatinum360Response.response.PersonGeneral.Nationality,
            '${estadoCivil}': data.platinum360.getInformePlatinum360Response.response.PersonGeneral.MaritalStatus,
            '${address}': data.platinum360.getInformePlatinum360Response.response.PersonGeneral.GeneralInformation.PersonGeneralInfo.Address.AddressType[0].address,
            '${ise}': data.platinum360.getInformePlatinum360Response.response.PersonGeneral.OtherPersonalData.ISE,
            '${pensionSolidaria}': data.platinum360.getInformePlatinum360Response.response.PersonGeneral.OtherPersonalData.PensionSolidaria,
            '${fonasa}': data.platinum360.getInformePlatinum360Response.response.PersonGeneral.OtherPersonalData.Fonasa,
            '${boletinConcursal}': data.platinum360.getInformePlatinum360Response.response.PersonGeneral.OtherPersonalData.BoletinConcursal,
            '${email}': data.platinum360.getInformePlatinum360Response.response.PersonGeneral.ContactInformation.Emails[0],
            '${web}': data.platinum360.getInformePlatinum360Response.response.PersonGeneral.ContactInformation.Web,
            '${rutConyuge}': formatearRUT(data.platinum360.getInformePlatinum360Response.response.PersonGeneral.SocietyData.Spouse.Rut),
            '${indicadorRiesgo}': data.platinum360.getInformePlatinum360Response.response.Credit.CreditRisk.PersonsRate,
            '${scoreColor}': data.platinum360.getInformePlatinum360Response.response.Credit.CreditRisk.PersonsRate,
            '${fechaCompleta}': fulldate,
            '${phoneItems}': Array.isArray(data.platinum360.getInformePlatinum360Response.response.PersonGeneral.Phones) ?
                data.platinum360.getInformePlatinum360Response.response.PersonGeneral.Phones.map(phone => `<div class="phone-item"><span class="phone-number">${phone}</span></div>`).join('') : '-',
            '${numeroSocios}': data.platinum360.getInformePlatinum360Response.response.PersonGeneral.SocietyData.Societies.length,
            '${numeroSociedades}': data.platinum360.getInformePlatinum360Response.response.PersonGeneral.SocietyData.Societies.length,
            '${listaSociedades}': Array.isArray(data.platinum360.getInformePlatinum360Response.response.PersonGeneral.SocietyData.Societies) ?
                data.platinum360.getInformePlatinum360Response.response.PersonGeneral.SocietyData.Societies.map(society =>
                    `<div class="society-item">${society.SocietyName} - ${society.Role}</div>`).join('')
                : '-',
            '${totalDocsLast6Months}': data.platinum360.getInformePlatinum360Response.response.Credit.DebtsSummary.TotalDocsLast6Months,
            '${evercleanIndicator}': data.platinum360.getInformePlatinum360Response.response.Credit.CreditRisk.Everclean,
            '${numberOfProperties}': data.platinum360.getInformePlatinum360Response.response.Properties.RealStateProperties.length,
            '${numberOfVehicles}': data.platinum360.getInformePlatinum360Response.response.Properties.Cars.length,
            '${actividadEconomica}': 'ACTIVIDADES DE PROGRAMACION INFORMATICA', // Ajustar según el dato correspondiente si se requiere
            '${fechaInicioActividades}': '0', // Este campo no está en el JSON actual; necesita lógica adicional
            '${fechaUltimoTimbraje}': data.platinum360.getInformePlatinum360Response.response.Credit.SBIF.SBIFDetail?.[0]?.Date || '-',
            '${fechaActualizacion}': '-',
            '${unpaidTotalAmountInMillions}': (data.platinum360.getInformePlatinum360Response.response.Credit.DebtsSummary.UnpaidTotalAmount / 1000000).toFixed(2),
            '${unpaidTotalNumber}': data.platinum360.getInformePlatinum360Response.response.Credit.DebtsSummary.UnpaidTotalNumber,
            '${rows}': Array.isArray(data.platinum360.getInformePlatinum360Response.response.Credit.ProtestDetail) ?
                data.platinum360.getInformePlatinum360Response.response.Credit.ProtestDetail.map(detail => `
        <tr>
            <td data-label="Vencimiento">${detail.Date}</td>
            <td data-label="Documento">${detail.ProtestType}</td>
            <td data-label="Monto">${detail.Amount}</td>
            <td data-label="Librador / Acreedor">${detail.LibradorName}</td>
            <td data-label="Moneda">${detail.MoneyCode}</td>
            <td data-label="Tipo de Deuda">${detail.DebtType}</td>
            <td data-label="Número de Cheque / Operación">${detail.ChequeOperatioNumber}</td>
            <td data-label="Publicación">${detail.ExpirationDate}</td>
            <td data-label="Notario">${detail.Notario}</td>
        </tr>        
    `).join('') : '-',
            '${rangoRenta}': data.platinum360.getInformePlatinum360Response.response.Capacity.Salary ?
                new Intl.NumberFormat('es-CL', { style: 'currency', currency: 'CLP', minimumFractionDigits: 0 }).format(data.platinum360.getInformePlatinum360Response.response.Capacity.Salary.RentaFuncionarioPublico ||
                    data.platinum360.getInformePlatinum360Response.response.Capacity.Salary.RentaPresunta || '-') : '-',

        };


    // Reemplazar todas las ocurrencias en el template
    htmlContent = htmlContent.replace(/\${(.*?)}/g, (match) => replacements[match] || '');

    // Convertir el HTML en Base64
    const htmlBase64 = Buffer.from(htmlContent).toString('base64');

    return {
        statusCode: 200,
        body: JSON.stringify({ base64HTML: htmlBase64 }),
    };

    } catch (error) {
        console.error('Error al procesar el reporte:', error);
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Internal server error' }),
        };
    }


};

function /* The `formatearRUT` function is formatting a Chilean RUT (Rol Único Tributario) number by
adding dots as thousand separators and appending the verification digit with a hyphen. */
    /* The `formatearRUT` function is formatting a Chilean RUT (Rol Único Tributario) number by
    adding dots as thousand separators and appending the verification digit with a hyphen. */
    formatearRUT(rut) {
    rut = rut.toString();
    const dv = rut.slice(-1);
    let numero = rut.slice(0, -1);
    numero = numero.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    return `${numero}-${dv}`;
}

function evaluarGenero(genero) {
    if (genero === 'F') {
        return 'Femenino';
    } else if (genero === 'M') {
        return 'Masculino';
    } else {
        return 'Desconocido';
    }
}

function getScoreColor(score) {
    if (score < 200) {
        return 'red'; // Crédito considerado como pérdida
    } else if (score >= 200 && score <= 399) {
        return 'orange'; // Crédito deficiente
    } else if (score >= 400 && score <= 599) {
        return 'yellow'; // Crédito de riesgo potencial
    } else if (score >= 600 && score <= 799) {
        return 'lightgreen'; // Crédito con riesgo mínimo
    } else {
        return 'green'; // Crédito prácticamente sin riesgo
    }
}
