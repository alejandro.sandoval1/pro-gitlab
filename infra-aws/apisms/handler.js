const AWS = require('aws-sdk');
const pinpoint = new AWS.Pinpoint({ region: process.env.AWS_REGION });

exports.apisms = async (event) => {
    const { phoneNumber, message } = JSON.parse(event.body);

    const params = {
        ApplicationId: process.env.APPLICATION_ID,
        MessageRequest: {
            Addresses: {
                [phoneNumber]: {
                    ChannelType: 'SMS'
                }
            },
            MessageConfiguration: {
                SMSMessage: {
                    Body: message,
                    MessageType: 'TRANSACTIONAL',
                    OriginationNumber: process.env.SMS_SENDER_ID
                }
            }
        }
    };

    try {
        await pinpoint.sendMessages(params).promise();
        return {
            statusCode: 200,
            body: JSON.stringify({ message: 'SMS enviado exitósamente' }),
        };
    } catch (error) {
        console.error('Error al enviar el SMS:', error);
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Error al enviar el SMS' }),
        };
    }
};
