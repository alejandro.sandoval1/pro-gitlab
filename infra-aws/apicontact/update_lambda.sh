#!/bin/bash

# Define variables
FUNCTION_NAME="arn:aws:lambda:us-east-1:487128762439:function:stackContact-ContactFunction-4r7Q7VGu7unp"
PROJECT_PATH="/Users/alejandroschwartz/space/mycreditscore/pro-gitlab/infra-aws/apicontact"
ZIP_FILE="lambda_function.zip"

# Navega al directorio del proyecto
cd $PROJECT_PATH

# Instala dependencias
npm install

# Crea el archivo ZIP
zip -r $ZIP_FILE handler.js assets node_modules package.json package-lock.json template.html

# Actualiza el código de la función Lambda
aws lambda update-function-code --function-name $FUNCTION_NAME --zip-file fileb://$ZIP_FILE

# Espera a que la función esté lista para ser actualizada
echo "Esperando a que la función esté lista para actualizar la configuración..."
while true; do
    STATUS=$(aws lambda get-function --function-name $FUNCTION_NAME --query 'Configuration.LastUpdateStatus' --output text)
    if [ "$STATUS" == "Successful" ] || [ "$STATUS" == "Failed" ]; then
        break
    fi
    echo "Estado actual: $STATUS. Esperando..."
    sleep 10
done

# Actualiza la configuración de la función Lambda solo si es necesario
RUNTIME_STATUS=$(aws lambda get-function-configuration --function-name $FUNCTION_NAME --query 'Runtime' --output text)
if [ "$RUNTIME_STATUS" != "nodejs16.x" ]; then
    aws lambda update-function-configuration --function-name $FUNCTION_NAME --runtime nodejs16.x
else
    echo "El runtime ya está configurado en nodejs16.x, no es necesario actualizarlo."
fi

echo "Actualización completada."
