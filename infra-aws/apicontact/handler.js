const AWS = require('aws-sdk');
const nodemailer = require('nodemailer');
const fs = require('fs');
const path = require('path');

AWS.config.update({ region: 'us-east-1' });
const ses = new AWS.SES();

exports.apicontact = async (event) => {
    if (!event.body) {
        return {
            statusCode: 400,
            body: JSON.stringify({ message: 'Entrada inválida' })
        };
    }

    const { message,recipient, subject, nombre } = JSON.parse(event.body);

    if (!recipient || !subject || !nombre || !message) {
        return {
            statusCode: 400,
            body: JSON.stringify({ message: 'Faltan parámetros' })
        };
    }

    try {
        const transporter = nodemailer.createTransport({
            SES: ses
        });

        const templatePath = path.join(__dirname, 'template.html');
        let htmlContent = fs.readFileSync(templatePath, 'utf8');
        htmlContent = htmlContent.replace('${nombre}', nombre);

        const mailOptions = {
            from: process.env.SENDER_EMAIL, // Cambia esto a tu correo verificado en SES
            to: recipient,
            subject: subject,
            text: message,
            html: htmlContent,
            attachments: []
        };
 

        const info = await transporter.sendMail(mailOptions);

        return {
            statusCode: 200,
            body: JSON.stringify({ message: 'Email enviado exitosamente!', info: info })
        };

    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Error enviando el email', error: error.message })
        };
    }
};
