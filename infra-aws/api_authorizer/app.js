const AWS = require('aws-sdk');
const cognito = new AWS.CognitoIdentityServiceProvider();

exports.handler = async (event) => {
    const { username, password } = JSON.parse(event.body);

    const userPoolId = 'us-east-1_Oyli5M2dP';  // Reemplazar con tu User Pool ID
    const clientId = '4hb70jqaf2gd0lj4dvg0sod7c0';  // Reemplazar con tu App Client ID

    const params = {
        AuthFlow: 'USER_PASSWORD_AUTH',
        AuthParameters: {
            USERNAME: username,
            PASSWORD: password
        },
        ClientId: clientId
    };

    try {
        const authResponse = await cognito.initiateAuth(params).promise();
        console.log('Auth response:', authResponse);

        const token = authResponse.AuthenticationResult.AccessToken;
        return {
            statusCode: 200,
            body: JSON.stringify({ token })
        };
    } catch (err) {
        console.error('Error:', err);
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Error al autenticar usuario' })
        };
    }
};
