#!/bin/bash

# Define variables
FUNCTION_NAME="arn:aws:lambda:us-east-1:487128762439:function:FunctionTrxPostNode"
PROJECT_PATH="/Users/alejandroschwartz/space/mycreditscore/pro-gitlab/infra-aws/transactionsAdd"
ZIP_FILE="lambda_function.zip"

# Navigate to project directory
cd $PROJECT_PATH

# Install dependencies
npm install

# Create ZIP file
zip -r $ZIP_FILE handler.js node_modules package.json package-lock.json

# Update Lambda function code
aws lambda update-function-code --function-name $FUNCTION_NAME --zip-file fileb://$ZIP_FILE

# Update Lambda function runtime (if needed)
aws lambda update-function-configuration --function-name $FUNCTION_NAME --runtime nodejs16.x
