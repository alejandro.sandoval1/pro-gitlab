const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
const { v4: uuidv4 } = require('uuid');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.transactions = async (event) => {
    let movimientoId;
    let operacionId;
    try {
        // Verificar que el cuerpo del evento existe y es un objeto
        let requestBody;
        if (event) {
            // Si el body está en formato de cadena JSON, se convierte a objeto
            requestBody = typeof event === 'string' ? JSON.parse(event) : event;
        } else {
            throw new Error("El cuerpo de la solicitud no está definido.");
        }

        // Extraer los parámetros del cuerpo
        const { tenant_id, client_id, producto_id, servicio_id, entidad, descripcion, status } = requestBody;

        // Validación de que todos los parámetros requeridos están presentes
        if (!tenant_id || !client_id || !producto_id || !servicio_id || !entidad || !descripcion || !status) {
            return {
                statusCode: 400,
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify({ error: 'Todos los parámetros son obligatorios.' })
            };
        }

        // Generar IDs únicos para el movimiento y la operación
        movimientoId = uuidv4();
        operacionId = uuidv4();
        const token_autorizacion = uuidv4();
        const fechaActual = new Date().toISOString();

        // Preparar parámetros para insertar el movimiento en la tabla Trx_Movimientos
        const movimientoParams = {
            TableName: 'TrxMovimientos',
            Item: {
                id: movimientoId,
                tenant_id: tenant_id,
                client_id: client_id,
                producto_id: producto_id,
                servicio_id: servicio_id,
                fecha: fechaActual,
                status: status,
                entidad: entidad
            }
        };

        // Insertar el movimiento en DynamoDB
        await dynamoDB.put(movimientoParams).promise();

        // Preparar parámetros para insertar la operación en la tabla Operacion
        const operacionParams = {
            TableName: 'Operacion',
            Item: {
                id: operacionId,
                id_movimiento: movimientoId,
                descripcion: descripcion,
                estado: status,
                token_autorizacion: token_autorizacion
            }
        };

        // Insertar la operación en DynamoDB
        await dynamoDB.put(operacionParams).promise();

        // Retornar una respuesta exitosa con los IDs generados
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify({
                mensaje: `Compra realizada exitosamente. ID Movimiento: ${movimientoId}, ID Operación: ${operacionId}`,
                id_movimiento: movimientoId,
                id_operacion: operacionId
            })
        };

    } catch (error) {
        console.error('Error al procesar la solicitud:', error);

        // Manejar el caso de error retornando los IDs generados (si existen)
        const errorMessage = {
            error: 'Ocurrió un error al procesar la solicitud.',
            id_movimiento: movimientoId || 'No disponible',
            id_operacion: operacionId || 'No disponible'
        };

        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(errorMessage)
        };
    }
};
