const AWS = require('aws-sdk');
const nodemailer = require('nodemailer');
const fs = require('fs');
const path = require('path');

exports.apimail = async (event) => {
    const ses = new AWS.SES({ region: process.env.REGION });

    try {
        const { recipient, subject, message, attachment } = JSON.parse(event.body);
        const attachmentData = Buffer.from(attachment, 'base64');

        const transporter = nodemailer.createTransport({
            SES: ses
        });

        const templatePath = path.join(__dirname, 'template.html');
        let htmlContent = fs.readFileSync(templatePath, 'utf8');
        htmlContent = htmlContent.replace('${message}', message);

        const mailOptions = {
            from: process.env.SENDER_EMAIL, // Cambia esto a tu correo verificado en SES
            to: recipient,
            subject: subject,
            text: message,
            html: htmlContent,
            attachments: [
                {
                    filename: 'attachment.pdf',
                    content: attachmentData
                },
                {
                    filename: 'logo.png',
                    path: path.join(__dirname, 'assets/Image@1x.png'), // Ruta correcta del logo
                    cid: 'logo' // mismo CID que se usa en el HTML
                }
            ]
        };

        const info = await transporter.sendMail(mailOptions);

        return {
            statusCode: 200,
            body: JSON.stringify({ message: 'Email sent successfully!', info: info })
        };

    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Error sending email', error: error.message })
        };
    }
};
