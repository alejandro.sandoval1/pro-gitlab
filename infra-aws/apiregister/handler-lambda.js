const AWS = require('aws-sdk');

AWS.config.update({region: 'us-east-1'});
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.apilogin = async (event) => {
    
    
    try {
        let username, password;
        // Intenta parsear event.body directamente
        
        
        try {
            username = JSON.stringify(event.username);
            password = JSON.stringify(event.password);
        } catch (error) {
            console.error('Error parsing JSON:', error);
            return {
                statusCode: 400,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify({ message: "Invalid JSON in request body" })
            };
        }

        const getUserParams = {
            TableName: 'Usuario',
            IndexName: 'username-index', // Reemplaza 'UsernameIndex' con el nombre de tu índice si es diferente
            KeyConditionExpression: 'username = :username',
            ExpressionAttributeValues: {
                ':username': JSON.parse(username)
            }
        };

        console.error(getUserParams);
        
        // Obtener usuario
        const userData = await dynamoDB.query(getUserParams).promise(); 
        
        
        if (userData.Items.length === 0) {
            return {
                statusCode: 404,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify({ message: "Usuario no encontrado" })
            };
        }

        const usuario = userData.Items[0]; // Aquí obtienes el primer usuario encontrado

        if (!usuario || usuario.password !== JSON.parse(password)) {
            return {
                statusCode: 401,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify({ message: "Unauthorized" })
            };
        }
        const usuarioId = usuario.id;
        // Obtener perfil
        const getPerfilParams = {
            TableName: 'Perfil',
            KeyConditionExpression: 'usuario_id = :usuario_id',
            ExpressionAttributeValues: {
                ':usuario_id': usuarioId
            }
        };

        const perfilData = await dynamoDB.query(getPerfilParams).promise();
        if (perfilData.Items.length === 0) {
            return {
                statusCode: 404,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify({ message: "Perfil not found" })
            };
        }

        const rolId = perfilData.Items[0].rol_id;

        // Obtener rol
        const getRolParams = {
            TableName: 'Rol',
            Key: {
                id: rolId
            }
        };

        const rolData = await dynamoDB.get(getRolParams).promise();
        if (!rolData.Item) {
            return {
                statusCode: 404,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify({ message: "Rol not found" })
            };
        }

        // Respuesta exitosa
        return {
            statusCode: 200,
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify({
                message: "Acceso OK",
                usuario: userData.Item,
                perfil: perfilData.Items[0],
                rol: rolData.Item
            })
        };
    } catch (err) {
        console.error(err);
        return {
            statusCode: 500,
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify({
                message: "Internal server error",
                errorMessage: err.message,
                errorStack: err.stack,
            })
        };
    }
    
    
};
