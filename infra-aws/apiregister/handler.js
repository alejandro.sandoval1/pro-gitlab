const AWS = require('aws-sdk');
const { v4: uuidv4 } = require('uuid');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const fs = require('fs');
const path = require('path');

AWS.config.update({ region: 'us-east-1' });
const dynamoDB = new AWS.DynamoDB.DocumentClient();
const ses = new AWS.SES(); 

exports.apiregister = async (event) => {
    try {
        let rut, nombre, apellido, email, fono, nrodocumento;

        // Intenta parsear event.body directamente
        try {
            if (!event.body) {
                throw new Error('Event body is missing');
            }

            console.log('Event body:', event.body); // Log para depurar

            const body = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

            ({ rut, nombre, apellido, email, fono, nrodocumento } = body);

            if (!rut || !nombre || !apellido || !email || !fono || !nrodocumento) {
                throw new Error('Missing required fields');
            }
        } catch (error) {
            console.error('Error parsing JSON:', error);
            return {
                statusCode: 400,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify({ message: "Invalid JSON in request body" })
            };
        }

        const id = uuidv4(); // Genera un UUID para el id
        const fecha_creacion = new Date().toISOString();
        const fecha_actualizacion = fecha_creacion;

        const putItemParams = {
            TableName: process.env.TABLE_NAME,
            Item: {
                id,
                rut,
                nombre,
                apellido,
                email,
                fono,
                fecha_creacion,
                fecha_actualizacion,
                nrodocumento
            }
        };

        await dynamoDB.put(putItemParams).promise();

        // Generar un UUID para el nuevo usuario
        const userId = uuidv4();
        const randomPassword = Math.random().toString(36).slice(-6);

        const putUserParams = {
            TableName: process.env.USER_TABLE_NAME, // Asegúrate de configurar esta variable de entorno
            Item: {
                id: userId,
                username: email,
                password: randomPassword,
                email: email,
                cliente_id: id,
                fecha_creacion,
                fecha_actualizacion
            }
        };

        await dynamoDB.put(putUserParams).promise();

        // Insertar un registro en la tabla Perfil
        const perfilParams = {
            TableName: process.env.PERFIL_TABLE_NAME, // Utiliza la variable de entorno para la tabla Perfil
            Item: {
                usuario_id: userId,
                rol_id: "uuid6",
                descripcion: "Perfil del usuario 0 con rol Visualizador"
            }
        };

        await dynamoDB.put(perfilParams).promise();


        const userParams2 = {
            TableName: 'Cliente',
            IndexName: 'email-index',
            KeyConditionExpression: 'email = :email',
            ExpressionAttributeValues: {
                ':email': email
            }
        };


        const userResult2 = await dynamoDB.query(userParams2).promise();
        if (!userResult2.Items.length) {
            return {
                statusCode: 404,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify({ message: 'Cliente no existe' })
            };
        } else {
            clienteName = userResult2.Items[0].nombre + " " + userResult2.Items[0].apellido;
        }

        // Enviar correo de notificación
        const transporter = nodemailer.createTransport({
            SES: ses
        });

        const templatePath = path.join(__dirname, 'template.html');
        let htmlContent = fs.readFileSync(templatePath, 'utf8');
        htmlContent = htmlContent.replace('${cliente}', clienteName);
        htmlContent = htmlContent.replace('${username}', email);
        htmlContent = htmlContent.replace('${password}', randomPassword);

        const mailOptions = {
            from: process.env.SENDER_EMAIL, // Cambia esto a tu correo verificado en SES
            to: email,
            subject: 'Acceso y Credenciales Iniciales',
            text: 'Tu contraseña ha sido actualizada exitosamente.',
            html: htmlContent,
            attachments: [
                {
                    filename: 'logo.png',
                    path: path.join(__dirname, 'assets', 'Image@1x.png'), // Ruta correcta del logo
                    cid: 'logo' // mismo CID que se usa en el HTML
                }
            ]
        };

        const info = await transporter.sendMail(mailOptions);

        // Respuesta exitosa
        return {
            statusCode: 201,
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify({
                message: "Cliente, usuario y perfil registrados exitosamente",
                clienteId: id,
                userId: userId,
                randomPassword: randomPassword ,
                infoMail: info
            })
        };
    } catch (err) {
        console.error(err);
        return {
            statusCode: 500,
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify({
                message: "Internal server error",
                errorMessage: err.message,
                errorStack: err.stack,
            })
        };
    }
};