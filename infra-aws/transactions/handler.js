const AWS = require('aws-sdk');

AWS.config.update({ region: 'us-east-1' });
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.transactions = async (event) => {
    const params = {
        TableName: 'TrxMovimientos'
    };

    try { 
        const data = await dynamoDB.scan(params).promise();
        const movimientos = data.Items;

        if (movimientos.length === 0) {
            return {
                statusCode: 404,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify({ error: 'No se encontraron transacciones.' })
            };
        }

        const movimientosEnriquecidos = [];

        for (const movimiento of movimientos) {
            let tenant = {}, servicio = {}, cliente = {}, producto = {}, tarifas = [];

            if (movimiento.tenant_id) {
                const tenantParams = {
                    TableName: 'Tenant',
                    IndexName: 'id-index',
                    KeyConditionExpression: 'id = :tenant_id',
                    ExpressionAttributeValues: { ':tenant_id': movimiento.tenant_id }
                };
                const tenantResponse = await dynamoDB.query(tenantParams).promise();
                tenant = tenantResponse.Count > 0 ? tenantResponse.Items[0] : null;
            }

            if (movimiento.client_id) {
                const clienteParams = {
                    TableName: 'Cliente',
                    IndexName: 'id-index',
                    KeyConditionExpression: 'id = :client_id',
                    ExpressionAttributeValues: { ':client_id': movimiento.client_id }
                };
                const clienteResponse = await dynamoDB.query(clienteParams).promise();
                cliente = clienteResponse.Count > 0 ? clienteResponse.Items[0] : null;
            }

            if (movimiento.producto_id) {
                const productoParams = {
                    TableName: 'Producto',
                    IndexName: 'id-index',
                    KeyConditionExpression: 'id = :producto_id',
                    ExpressionAttributeValues: { ':producto_id': movimiento.producto_id }
                };
                const productoResponse = await dynamoDB.query(productoParams).promise();
                producto = productoResponse.Count > 0 ? productoResponse.Items[0] : null;
            }

            if (movimiento.servicio_id) {
                const servicioParams = {
                    TableName: 'Servicio',
                    IndexName: 'id-index',
                    KeyConditionExpression: 'id = :servicio_id',
                    ExpressionAttributeValues: { ':servicio_id': movimiento.servicio_id }
                };
                const servicioResponse = await dynamoDB.query(servicioParams).promise();
                servicio = servicioResponse.Count > 0 ? servicioResponse.Items[0] : null;

                const tarifaParams = {
                    TableName: 'Tarifa',
                    IndexName: 'producto_id-index',
                    KeyConditionExpression: 'producto_id = :prod_id',
                    ExpressionAttributeValues: { ':prod_id': movimiento.producto_id }
                };
                const tarifaResponse = await dynamoDB.query(tarifaParams).promise();
                tarifas = tarifaResponse.Count > 0 ? tarifaResponse.Items : [];
            }

            movimientosEnriquecidos.push({
                tenant_id: movimiento.tenant_id,
                tenant_nombre: tenant ? tenant.nombre : 'Nombre del tenant no encontrado',
                tenant_apellido: tenant ? tenant.apellido : 'Apellido del tenant no encontrado',
                tenant_email: tenant ? tenant.email : 'Email del tenant no encontrado',
                cliente_rut: cliente ? cliente.rut : 'RUT del cliente no encontrado',
                cliente_nombre: cliente ? `${cliente.nombre} ${cliente.apellido}` : 'Nombre del cliente no encontrado',
                cliente_apellido: cliente ? cliente.apellido : 'Apellido del cliente no encontrado',
                cliente_email: cliente ? cliente.email : 'Email del cliente no encontrado',
                producto_id: movimiento.producto_id,
                producto_descripcion: producto ? producto.descripcion : 'Descripción del producto no encontrada',
                servicio_id: movimiento.servicio_id,
                servicio_descripcion: servicio ? servicio.descripcion : 'Descripción del servicio no encontrada',
                tarifas: tarifas.map(t => ({ monto: t.monto, moneda: t.moneda })),
                fecha_movimiento: movimiento.fecha,
                estado: movimiento.estado,
                entidad: movimiento.entidad
            });
        }

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify(movimientosEnriquecidos)
        };
    } catch (err) {
        console.error('Error al procesar la solicitud:', err);
        return {
            statusCode: 500,
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify({
                message: "Failed to query transactions",
                errorMessage: err.message,
                errorStack: err.stack,
            })
        };
    }
};
