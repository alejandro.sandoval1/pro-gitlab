const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB.DocumentClient();

exports.items = async (event) => {
    try {
        console.log("Obtener todos los registros de la tabla Producto_Servicio");
        const productoServicioParams = {
            TableName: 'ProductoServicio'
        };

        const productoServicioResponse = await dynamodb.scan(productoServicioParams).promise();
        const productoServicioItems = productoServicioResponse.Items;

       console.log("Verificar si hay registros");
        if (productoServicioItems.length === 0) {
            return {
                statusCode: 404,
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ error: 'No se encontraron relaciones Producto_Servicio.' })
            };
        }

        console.log("Obtener las descripciones y tarifas de cada producto relacionado");
        const productosConDetalles = await Promise.all(productoServicioItems.map(async (item) => {
            console.log(" Consultar la tabla Producto para obtener la descripción");
            const productoParams = {
                TableName: 'Producto',
                IndexName: 'id-index',
                KeyConditionExpression: 'id = :prod_id',
                ExpressionAttributeValues: {
                    ':prod_id': item.producto_id
                }
            };

            const productoResponse = await dynamodb.query(productoParams).promise();

            console.log("Consultar la tabla Tarifa para obtener la tarifa y moneda");
            const tarifaParams = {
                TableName: 'Tarifa',
                KeyConditionExpression: 'servicio_id = :serv_id',
                ExpressionAttributeValues: {
                    ':serv_id': item.servicio_id
                }
            };

            const tarifaResponse = await dynamodb.query(tarifaParams).promise();

            let descripcion = 'Descripción no encontrada.';
            let tarifa = 'Tarifa no encontrada.';
            let moneda = 'Moneda no encontrada.';

           console.log("Si se encuentra el producto, obtener la descripción");
            if (productoResponse.Count > 0) {
                descripcion = productoResponse.Items[0].descripcion;
            }

            console.log("Si se encuentra la tarifa, obtener la tarifa y moneda");
            if (tarifaResponse.Count > 0) {
                tarifa = tarifaResponse.Items[0].monto;
                moneda = tarifaResponse.Items[0].moneda;
            }

            return {
                producto_id: item.producto_id,
                servicio_id: item.servicio_id,
                descripcion,
                tarifa,
                moneda
            };
        }));

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(productosConDetalles)
        };

    } catch (error) {
        console.error('Error al procesar la solicitud:', error);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ error: 'Ocurrió un error al procesar la solicitud.' })
        };
    }
};
