# Informe de Persona

Este código proporciona una función llamada `informePersona` que se utiliza para obtener un resumen del informe crediticio de un consumidor a través de la API de Equifax Sandbox. La función está diseñada para ser utilizada dentro de un entorno de servidor sin estado, como AWS Lambda.

## Requisitos

- Node.js instalado en tu sistema.
- Axios, una biblioteca para realizar solicitudes HTTP desde Node.js. Puedes instalarlo ejecutando `npm install axios`.

## Uso

1. Copia el código proporcionado en un archivo JavaScript, como `informePersona.js`.
2. Asegúrate de tener una cuenta en la API de Equifax Sandbox y obtén un token de acceso válido.
3. Reemplaza el token de acceso en el código con tu propio token.
4. Ejecuta la función según sea necesario.

## Configuración

El código utiliza Axios para realizar solicitudes HTTP. Se configuran dos solicitudes GET a la misma URL con diferentes tokens de autorización en el encabezado. Puedes ajustar la configuración según tus necesidades.

## Manejo de Errores

El código maneja los errores de manera adecuada. Si la solicitud falla, devolverá un objeto JSON con un mensaje de error y un código de estado correspondiente.

## Contribución

Siéntete libre de contribuir con mejoras a este código a través de solicitudes de extracción. ¡Toda contribución es bienvenida!

## Licencia

Este código se proporciona bajo la [Licencia MIT](LICENSE).
