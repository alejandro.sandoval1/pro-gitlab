// handler.js (ubicado en ./apiprotected)

const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event) => {
    // Obtener datos del evento de la solicitud API Gateway
    console.log('Evento recibido:', JSON.stringify(event));

    try {
        // Aquí puedes implementar lógica específica de tu función
        // En este caso, se asume que se va a consultar la tabla ClienteTable
        const params = {
            TableName: process.env.TABLE_NAME, // Se obtiene el nombre de la tabla desde las variables de entorno
            Key: {
                id: '1', // Ejemplo de cómo podría ser la consulta, adaptar según tu lógica real
            },
        };

        const data = await dynamoDB.get(params).promise();
        console.log('Resultado de consulta:', JSON.stringify(data));

        return {
            statusCode: 200,
            body: JSON.stringify(data.Item),
            headers: {
                'Content-Type': 'application/json',
            },
        };
    } catch (error) {
        console.error('Error al procesar la solicitud:', error);
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Error al procesar la solicitud' }),
            headers: {
                'Content-Type': 'application/json',
            },
        };
    }
};
