const AWS = require('aws-sdk');
const ses = new AWS.SES({ region: 'us-east-1' });

exports.apiactive = async (event) => {
    try {
        const body = JSON.parse(event.body);
        const email = body.email;

        const params = {
            EmailAddress: email,
        };

        await ses.verifyEmailIdentity(params).promise();

        return {
            statusCode: 200,
            body: JSON.stringify(`Activation email sent to ${email}`)
        };
    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify(`Error: ${error.message}`)
        };
    }
};