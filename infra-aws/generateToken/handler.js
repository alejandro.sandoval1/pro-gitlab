const AWS = require('aws-sdk');
const { v4: uuidv4 } = require('uuid');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const fs = require('fs');
const path = require('path');

AWS.config.update({region: 'us-east-1'});
const dynamoDB = new AWS.DynamoDB.DocumentClient();
const ses = new AWS.SES(); // Inicializa el servicio SES

exports.generateToken = async (event) => {
    let email;
    let clienteName;
    try {
        ({ email } = JSON.parse(event.body));
        console.log(email);
    } catch (error) {
        console.error('Error parsing JSON:', error);
        return {
            statusCode: 400,
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify({ message: "Invalid JSON in request body" })
        };
    }

    const userParams = {
        TableName: 'Usuario',
        IndexName: 'email-index',
        KeyConditionExpression: 'email = :email',
        ExpressionAttributeValues: {
            ':email': email
        }
    };

    const userParams2 = {
        TableName: 'Cliente',
        IndexName: 'email-index',
        KeyConditionExpression: 'email = :email',
        ExpressionAttributeValues: {
            ':email': email
        }
    };

    try {
        const userResult = await dynamoDB.query(userParams).promise();
        if (!userResult.Items.length) {
            return {
                statusCode: 404,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify({ message: 'Usuario no existe' })
            };
        }

        const id = uuidv4();
        const userId = userResult.Items[0].id;
        const tokenId = uuidv4();
        const tokenValue = crypto.randomBytes(3).toString('hex').toUpperCase();
        const createdAt = new Date().toISOString();
        const expiresAt = new Date(Date.now() + 5 * 60 * 1000).toISOString();

        const tokenParams = {
            TableName: 'Tokens',
            Item: {
                id,
                userId,
                tokenId,
                tokenValue,
                createdAt,
                expiresAt
            }
        };

        await dynamoDB.put(tokenParams).promise();


        const userResult2 = await dynamoDB.query(userParams2).promise();
        if (!userResult2.Items.length) {
            return {
                statusCode: 404,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify({ message: 'Cliente no existe' })
            };
        } else {
            clienteName = userResult2.Items[0].nombre + " " + userResult2.Items[0].apellido;
        }


        // Enviar correo de notificación
        const transporter = nodemailer.createTransport({
            SES: ses
        });

        const templatePath = path.join(__dirname, 'template.html');
        let htmlContent = fs.readFileSync(templatePath, 'utf8');
        htmlContent = htmlContent.replace('${cliente}', clienteName);
        htmlContent = htmlContent.replace('${token}', tokenValue);

        const mailOptions = {
            from: process.env.SENDER_EMAIL,
            to: email,
            subject: 'Notificación de Actualización de Contraseña',
            text: 'Tu contraseña ha sido actualizada exitosamente.',
            html: htmlContent,
            attachments: [
                {
                    filename: 'logo.png',
                    path: path.join(__dirname, 'assets/Image@1x.png'),
                    cid: 'logo'
                }
            ]
        };

        const info = await transporter.sendMail(mailOptions);

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify({ userId, tokenId, tokenValue, message: 'Usuario actualizado y correo enviado exitosamente!', info: info })
        };
    } catch (error) {
        console.error('Error:', error);
        return {
            statusCode: 500,
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify({ message: 'Error interno del servidor' })
        };
    }
};
